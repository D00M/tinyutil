#include "util.h"
#include <time.h>

struct timespec timespec_from_milliseconds(uint32_t msecs) {
    struct timespec time = {0, 0};

    time.tv_nsec = (msecs % 1000) * 1000000L;
    time.tv_sec = msecs / 1000;
    if (time.tv_nsec >= 1000000000L) {
        time.tv_nsec -= 1000000000L;
        time.tv_sec += 1;
    }

    return time;
}
