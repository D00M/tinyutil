#include <string.h>
#include <stdlib.h>
#include <utl/unsized_vector.h>

static bool utl_unsized_vector_resize(struct utl_unsized_vector *vector, size_t bytes) {
    if (!vector || !bytes) return false;
    if (vector->allocated > vector->size + bytes) return true;

    size_t allocated = vector->allocated ? vector->allocated : bytes * 4;

    while (allocated < vector->allocated + bytes) allocated *= 2;

    void *data;
    if (vector->allocated) {
        data = realloc(vector->data, allocated);
    } else {
        data = malloc(allocated);
    }

    if (!data) return false;

    vector->data = data;
    vector->allocated = allocated;

    return true;
}

bool utl_unsized_vector_init(struct utl_unsized_vector *vector) {
    if (!vector) return false;

    memset(vector, 0, sizeof(*vector));

    return true;
}

bool utl_unsized_vector_deinit(struct utl_unsized_vector *vector) {
    if (!vector) return false;

    free(vector->data);

    return true;
}

bool utl_unsized_vector_copy(struct utl_unsized_vector *vector, const struct utl_unsized_vector *source) {
    if (!vector || !source) return false;

    void *new = utl_unsized_vector_grow(vector, vector->size, source->size);
    if (!new) return false;

    mempcpy(new, source->data, source->size);

    return true;
}

void *utl_unsized_vector_grow(struct utl_unsized_vector *vector, size_t at, size_t bytes) {
    if (!vector || !bytes || at > vector->size || !utl_unsized_vector_resize(vector, bytes)) return NULL; 


    if (at != vector->size) {
        memmove(vector->data + at + bytes, vector->data + at, vector->size - at);
    }

    vector->size += bytes;

    return vector->data + at;
}

void *utl_unsized_vector_add(struct utl_unsized_vector *vector, size_t bytes) {
    return utl_unsized_vector_grow(vector, vector->size, bytes);
}

bool utl_unsized_vector_shrink(struct utl_unsized_vector *vector, size_t at, size_t bytes) {
    if (!vector || !bytes || bytes > vector->size || at > vector->size - bytes) return false;

    if (at != vector->size - bytes) {
        memmove(vector->data + at, vector->data + at + bytes, vector->size - at - bytes);
    }

    vector->size -= bytes;

    return true;
}

bool utl_unsized_vector_remove(struct utl_unsized_vector *vector, size_t bytes) {
    return utl_unsized_vector_shrink(vector, vector->size - bytes, bytes);
}
