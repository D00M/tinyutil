#include <utl/ordered_vector.h>
#include "util.h"
#include "utl/vector.h"

static uint32_t quick_sort_partition(struct utl_vector *vector, utl_cmp_func cmp, uint32_t min, uint32_t max);
static void quick_sort(struct utl_vector *vector, utl_cmp_func cmp, uint32_t min, uint32_t max);

static const struct utl_vector_sort_interface sorting_interface = {
    .search = utl_vector_binary_search,
    .sort = utl_vector_quick_sort,
    .sort_insert = utl_vector_insertion_sort,
};

bool utl_ordered_vector_init(struct utl_ordered_vector *vector, utl_cmp_func cmp, size_t element_size) {
    if (!vector || !cmp) return false;

    vector->interface = &sorting_interface;
    vector->cmp = cmp;
    return utl_vector_init(&vector->vector, element_size);
}

bool utl_ordered_vector_deinit(struct utl_ordered_vector *vector) {
    if (!vector) return false;

    return utl_vector_deinit(&vector->vector);
}

bool utl_ordered_vector_elements_deinit(struct utl_ordered_vector *vector, utl_destroy_func destroy) {
    if (!vector || !destroy) return false;

    return utl_vector_elements_deinit(&vector->vector, destroy);
}

bool utl_ordered_vector_attach_interface(struct utl_ordered_vector *vector, const struct utl_vector_sort_interface *interface) {
    if (!vector || !interface || !interface->sort || !interface->search || !interface->sort_insert) return false;

    vector->interface = interface;

    return true;
}

void *utl_ordered_vector_push(struct utl_ordered_vector *vector, const void *data) {
    if (!vector || !vector->interface->sort_insert) return NULL;

    void *new = utl_vector_push(&vector->vector, data);
    if (!new) return NULL;

    int32_t new_index = vector->interface->sort_insert(&vector->vector, vector->cmp, utl_vector_length(&vector->vector) - 1);

    new = utl_ordered_vector_get(vector, new_index);

    return new;
}

void *utl_ordered_vector_get(const struct utl_ordered_vector *vector, uint32_t index) {
    if (!vector) return NULL;

    return utl_vector_get(&vector->vector, index);
}

bool utl_ordered_vector_remove(struct utl_ordered_vector *vector, uint32_t index) {
    if (!vector) return false;

    return utl_vector_remove(&vector->vector, index);
}

int32_t utl_ordered_vector_search(const struct utl_ordered_vector *vector, const void *data) {
    if (!vector || !vector->interface->search) return -1;

    return vector->interface->search(&vector->vector, vector->cmp, data);
}

bool utl_ordered_vector_sort(struct utl_ordered_vector *vector) {
    if (!vector || !vector->interface->sort) return false;

    vector->interface->sort(&vector->vector, vector->cmp);

    return true;
}

uint32_t quick_sort_partition(struct utl_vector *vector, utl_cmp_func cmp, uint32_t min, uint32_t max) {
    void *pivot_value = utl_vector_get(vector, max);
    int32_t i = min - 1;

    for (uint32_t j = min; j <= max; j++) {
        void *value = utl_vector_get(vector, j);

        if (CMP_SMALLER(cmp(value, pivot_value))) {
            i++;
            utl_vector_swap(vector, i, j);
        }

    }

    utl_vector_swap(vector, i+1, max);
    return i+1;
}

void quick_sort(struct utl_vector *vector, utl_cmp_func cmp, uint32_t min, uint32_t max) {
    if (min >= max) return;

    uint32_t partition_index = quick_sort_partition(vector, cmp, min, max);
    quick_sort(vector, cmp, min, partition_index - 1);
    quick_sort(vector, cmp, partition_index + 1, max);
}

void utl_vector_quick_sort(struct utl_vector *vector, utl_cmp_func cmp) {
    if (!vector || !cmp) return;
    quick_sort(vector, cmp, 0, utl_vector_length(vector) - 1);
}

int32_t utl_vector_insertion_sort(struct utl_vector *vector, utl_cmp_func cmp, int32_t index) {
    if (!cmp || !vector || index >= utl_vector_length(vector)) return -1;

    uint32_t i, n, x, new_index = index;
    if (index == 0) i = index + 1;
    else if (index == -1) i = 1;
    else i = index;

    for (; i < utl_vector_length(vector); i++) {
        n = i - 1;
        x = i;

        while (CMP_SMALLER(cmp(utl_vector_get(vector, x), utl_vector_get(vector, n)))) {
            utl_vector_swap(vector, x, n);
            if (n == 0) break;
            if (new_index != -1 && i == index) new_index--;
            n--;
            x--;
        }
    }

    return new_index == -1 ? -1 : new_index;
}

// We need to do this manually because we want an index not a value.
int32_t utl_vector_binary_search(const struct utl_vector *vector, utl_cmp_func cmp, const void *data) {
    if (!vector || !cmp) return -1;

    uint32_t min = 0, max = utl_vector_length(vector) - 1, mid;
    int64_t cmp_val;
    void *current;

    while (min <= max) {
        mid = min + ((max - min) / 2);

        current = utl_vector_get(vector, mid);
        cmp_val = cmp(data, current);

        if (CMP_EQUAL(cmp_val)) return mid;

        if (CMP_LARGER(cmp_val)) {
            min = mid + 1;
        }
        else {
            max = mid - 1;
        }
    }

    return -1;
}
