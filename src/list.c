#include <utl/list.h>
#include <stdint.h>

bool utl_list_init(struct utl_list *list) {
    if (!list) return false;
    list->next = list;
    list->prev = list;
    return true;
}

bool utl_list_insert(struct utl_list *list, struct utl_list *element) {
    if (!list || !list->next || !list->prev || !element) return false;

    element->next = list->next;
    element->prev = list;
    list->next->prev = element;
    list->next = element;

    return true;
}

bool utl_list_remove(struct utl_list *element) {
    if (!element || !element->prev || !element->next) return false;

    element->next->prev = element->prev;
    element->prev->next = element->next;
    element->next = NULL;
    element->prev = NULL;

    return true;
}

int32_t utl_list_find(struct utl_list *list, struct utl_list *element) {
    if (!list || !list->next || !element) return -1;

    uint32_t index = 0;
    struct utl_list *pos = list->next;
    while (pos != list && pos != element) {
        pos = pos->next ? pos->next : list;
        index++;
    }

    return pos == list ? -1 : index;
}

struct utl_list *utl_list_at(struct utl_list *list, uint32_t index) {
    if (!list) return NULL;

    struct utl_list *element = list->next;
    while (element != list && index > 0) {
        element = element->next;
        index--;
    }

    return element == list ? NULL : element;
}

int32_t utl_list_length(struct utl_list *list) {
    if (!list || !list->next || !list->prev) return -1;
    uint32_t len = 0;
    struct utl_list *current = list->next;
    while (current != list) {
        len++;
        current = current->next;
    }
    return len;
}

bool utl_list_empty(struct utl_list *list) {
    return list && list->next == list && list->prev == list;
}

bool utl_list_insert_list(struct utl_list *list, struct utl_list *other) {
    // NOTE: We could do better checks like making sure these nodes are not within eachother's
    // list but that could make things much slower. Going from an O(1) operation to O(n).
    if (!list || !other) return false;
    if (utl_list_empty(other)) return true;

    other->next->prev = list;
    other->prev->next = list->next;
    list->next->prev = other->prev;
    list->next = other->next;

    return true;
}

bool utl_list_iterate_mutable(struct utl_list *list, utl_list_iterator_func iterator_func, void *data) {
    if (!list || !iterator_func) return false;

    struct utl_list cursor, end;

    utl_list_insert(list, &cursor);
    utl_list_insert(list->prev, &end);

    while (cursor.next != &end) {
        struct utl_list *current = cursor.next;

        utl_list_remove(&cursor);
        utl_list_insert(current, &cursor);

        if (iterator_func(current, data)) break;
    };

    utl_list_remove(&cursor);
    utl_list_remove(&end);

    return true;
}
