#include <utl/hash_table.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "util.h"

static void hash_table_rehash(struct utl_hash_table *table, uint32_t new_hash_table_size);
static struct utl_hash_table_entry *hash_table_get_entry(const struct utl_hash_table *table, uint32_t hash);

static const uint32_t deleted_entry;
static const struct hash_table_size {
   uint32_t max_entries, size, rehash;
} hash_sizes[] = {
    { 2,		    5,		        3	         },
    { 4,		    7,		        5	         },
    { 8,		    13,		        11	         },
    { 16,		    19,		        17	         },
    { 32,		    43,		        41           },
    { 64,		    73,		        71           },
    { 128,		    151,		    149          },
    { 256,		    283,		    281          },
    { 512,		    571,		    569          },
    { 1024,		    1153,		    1151         },
    { 2048,		    2269,		    2267         },
    { 4096,		    4519,		    4517         },
    { 8192,		    9013,		    9011         },
    { 16384,		18043,		    18041        },
    { 32768,		36109,		    36107        },
    { 65536,		72091,		    72089        },
    { 131072,		144409,		    144407       },
    { 262144,		288361,		    288359       },
    { 524288,		576883,		    576881       },
    { 1048576,		1153459,	    1153457      },
    { 2097152,		2307163,	    2307161      },
    { 4194304,		4613893,	    4613891      },
    { 8388608,		9227641,	    9227639      },
    { 16777216,		18455029,	    18455027     },
    { 33554432,		36911011,	    36911009     },
    { 67108864,		73819861,	    73819859     },
    { 134217728,	147639589,	    147639587    },
    { 268435456,	295279081,	    295279079    },
    { 536870912,	590559793,	    590559791    },
    { 1073741824,	1181116273,	    1181116271   },
    { 2147483648ul,	2362232233ul,	2362232231ul }
};

void hash_table_rehash(struct utl_hash_table *table, uint32_t new_hash_table_size) {
    if (!table || new_hash_table_size >= ARRAY_LENGTH(hash_sizes)) return;

    const struct hash_table_size *table_size = &hash_sizes[new_hash_table_size];
    struct utl_hash_table old = *table;
    struct utl_hash_table_entry *entry;

    table->entries = calloc(table_size->size, sizeof(*table->entries));
    if (!table->entries) return;

    table->hash_sizes_index = new_hash_table_size;
    table->allocated = table_size->size;
    table->deleted = 0;
    table->amnt = 0;

    for (entry = old.entries; entry != old.entries + old.allocated; entry++) {
        if (utl_hash_table_entry_is_filled(entry)) utl_hash_table_insert(table, entry->hash, entry->data);
    }

    free(old.entries);
}

struct utl_hash_table_entry *hash_table_get_entry(const struct utl_hash_table *table, uint32_t hash) {
    if (!table) return NULL;

    uint32_t hash_index = hash % table->allocated, double_hash;
    struct utl_hash_table_entry *entry;
    do {
        entry = table->entries + hash_index;
        if (entry && entry->data == NULL) {
            return NULL;
        }
        if (!utl_hash_table_entry_is_filled(entry) || entry->hash != hash) {
            double_hash = 1 + hash % hash_sizes[table->hash_sizes_index].rehash;
            hash_index = (hash_index + double_hash) % table->allocated;
            continue;
        }

        return entry;
    } while (hash_index != hash % table->allocated);

    return NULL;
}

bool utl_hash_table_init(struct utl_hash_table *table) {
    if (!table) return false;

    memset(table, 0, sizeof(*table));

    table->allocated = hash_sizes[0].size;
    table->entries = calloc(table->allocated, sizeof(*table->entries));
    if (!table->entries) return false;

    return true;
}

bool utl_hash_table_deinit(struct utl_hash_table *table) {
    if (!table) return false;

    free(table->entries);
    return true;
}

bool utl_hash_table_elements_deinit(struct utl_hash_table *table, utl_destroy_func destroy) {
    if (!table || !destroy) return false;

    for (struct utl_hash_table_entry *entry = table->entries; entry != table->entries + table->amnt; entry++) {
        if (!utl_hash_table_entry_is_filled(entry)) continue;

        destroy(entry->data);
    }

    utl_hash_table_deinit(table);
    return true;
}

bool utl_hash_table_insert(struct utl_hash_table *table, uint32_t hash, void *data) {
    if (!table || !data) return false;

    const struct hash_table_size *table_size = &hash_sizes[table->hash_sizes_index];
    if (table->amnt >= table_size->max_entries) {
        hash_table_rehash(table, ++table->hash_sizes_index);
    }
    else if (table->amnt + table->deleted >= table_size->max_entries) {
        hash_table_rehash(table, table->hash_sizes_index);
    }

    uint32_t hash_index = hash % table->allocated, double_hash;
    struct utl_hash_table_entry *entry;
    do {
        entry = table->entries + hash_index;
        if (utl_hash_table_entry_is_filled(entry)) {
            double_hash = 1 + hash % hash_sizes[table->hash_sizes_index].rehash;
            hash_index = (hash_index + double_hash) % table->allocated;
            continue;
        }

        if (utl_hash_table_entry_is_deleted(entry)) table->deleted--;
        else if (entry->hash == hash) break;

        entry->hash = hash;
        entry->data = data;
        table->amnt++;
        return true;
    } while (hash_index != hash % table->allocated);

    return false;
}

void *utl_hash_table_remove(struct utl_hash_table *table, uint32_t hash) {
    if (!table) return NULL;

    struct utl_hash_table_entry *entry = hash_table_get_entry(table, hash);
    if (!entry) return NULL;

    void *data = entry->data;

    entry->data = (void*)&deleted_entry;
    table->deleted++;

    return data;
}

void *utl_hash_table_get(const struct utl_hash_table *table, uint32_t hash)  {
    if (!table) return NULL;

    struct utl_hash_table_entry *entry = hash_table_get_entry(table, hash);
    if (!entry) return NULL;

    return entry->data;
}


bool utl_hash_table_entry_is_filled(const struct utl_hash_table_entry *entry) {
    return entry && entry->data != NULL && entry->data != &deleted_entry;
}

bool utl_hash_table_entry_is_deleted(const struct utl_hash_table_entry *entry) {
    return entry && entry->data == &deleted_entry;
}
