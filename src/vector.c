#include <utl/common.h>
#include <utl/vector.h>
#include <utl/unsized_vector.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

// This feels dumb
#define INDEX(V, index) ((V)->element_size * (index))
#define VEC_END(V) ((V)->vec.data + (V)->vec.size)

bool utl_vector_init(struct utl_vector *vector, size_t element_size)  {
    if (!vector || !element_size) return false;

    memset(vector, 0, sizeof(*vector));

    vector->element_size = element_size;

    return true;
}

bool utl_vector_deinit(struct utl_vector *vector) {
    return vector && utl_unsized_vector_deinit(&vector->vec);
}

bool utl_vector_elements_deinit(struct utl_vector *vector, utl_destroy_func destroy)  {
    if (!vector || !destroy) return false;

    for (uint8_t *ptr = vector->vec.data; ptr != VEC_END(vector); ptr += vector->element_size) {
        destroy(ptr);
    }

    return utl_vector_deinit(vector);
}

uint32_t utl_vector_length(const struct utl_vector *vector) {
    return vector->vec.size / vector->element_size;
}

bool utl_vector_copy(struct utl_vector *vector, const struct utl_vector *source) {
    return vector && source && utl_unsized_vector_copy(&vector->vec, &source->vec);
}

void *utl_vector_grow(struct utl_vector *vector, uint32_t index, uint32_t amnt) {
    if (!vector || !amnt || index > utl_vector_length(vector)) return NULL;

    return utl_unsized_vector_grow(&vector->vec, vector->element_size * index, vector->element_size * amnt);
}

void *utl_vector_add(struct utl_vector *vector) {
    return vector ? utl_vector_grow(vector, utl_vector_length(vector), 1) : NULL;
}

void *utl_vector_insert(struct utl_vector *vector, uint32_t index, const void *data) {
    if (!vector || !data) return NULL;

    void *new = utl_vector_grow(vector, index, 1);
    if (!new) return NULL;

    return memcpy(new, data, vector->element_size);
}

void *utl_vector_push(struct utl_vector *vector, const void *data) {
    return vector ? utl_vector_insert(vector, utl_vector_length(vector), data) : NULL;
}

void *utl_vector_get(const struct utl_vector *vector, uint32_t index) {
    if (!vector || index >= utl_vector_length(vector)) return NULL;

    return vector->vec.data + (vector->element_size * index);
}

bool utl_vector_shrink(struct utl_vector *vector, uint32_t index, uint32_t amnt) {
    if (!vector || !amnt || amnt > utl_vector_length(vector) || index > utl_vector_length(vector) - amnt) return false;

    return utl_unsized_vector_shrink(&vector->vec, vector->element_size * index, vector->element_size * amnt);
}

bool utl_vector_remove(struct utl_vector *vector, uint32_t index) {
    return vector && utl_vector_shrink(vector, index, 1);
}

bool utl_vector_swap(struct utl_vector *vector, uint32_t index1, uint32_t index2) {
    if (!vector || index1 == index2 || index1 >= utl_vector_length(vector) || index2 >= utl_vector_length(vector)) return false;

    // PERF: For the purposes of sorting this feels really inefficient.
    // I would like to find a way to do this with
    // just the two blocks of memory we have.
    // I know it's possible to swap two variables with XOR,
    // but I'm not sure how to apply that here.
    // I also don't know if XORing each byte would be more efficient
    // than just having another variable.
    uint8_t tmp[vector->element_size];
    memcpy(tmp, utl_vector_get(vector, index1), vector->element_size);
    memmove(utl_vector_get(vector, index1), utl_vector_get(vector, index2), vector->element_size);
    memmove(utl_vector_get(vector, index2), tmp, vector->element_size);

    return true;
}

int32_t utl_vector_cmp_find(const struct utl_vector *vector, utl_cmp_func cmp, const void *data) {
    if (!vector || !cmp) return -1;

    for (uint8_t *ptr = vector->vec.data; ptr != VEC_END(vector); ptr += vector->element_size) {
        if (cmp(ptr, data) == 0) return ((void*)ptr - vector->vec.data) / vector->element_size;
    }

    return -1;
}
