#include <stddef.h>
#include <stdint.h>
#include <string.h>

#define HASH_SEED 80085 // (:D

static uint32_t rotl32 (uint32_t x, int8_t r) {
    return (x << r) | (x >> (32 - r));
}

static uint32_t fmix32 (uint32_t h) {
    h ^= h >> 16;
    h *= 0x85ebca6b;
    h ^= h >> 13;
    h *= 0xc2b2ae35;
    h ^= h >> 16;
    return h;
}

/*
 * Thanks to Austin Appleby for murmur3 being public domain.
 * And thanks to Peter Scott for porting the code to C and adding some
 * documentation.
 *
 * https://github.com/aappleby/smhasher/tree/master - smhasher aka murmur3 repo from Austin Appleby.
 * https://github.com/PeterScott/murmur3 - murmur3 C port from Peter Scott.
 */
uint32_t utl_murmur3_hash(const void *key, size_t len, uint32_t seed) {
    if (!key) return 0;

    uint32_t h1 = seed,
             c1 = 0xcc9e2d51,
             c2 = 0x1b873593,
             k1;
    const uint8_t *data = (const uint8_t*)key;
    const int nblocks = len / 4;
    int i;

    const uint32_t *blocks = (const uint32_t *)(data + nblocks * 4);

    for (i = -nblocks; i; i++) {
        k1 = blocks[i];

        k1 *= c1;
        k1 = rotl32(k1, 15);
        k1 *= c2;

        h1 ^= k1;
        h1 = rotl32(h1, 13);
        h1 = h1 * 5 + 0xe6546b64;
    }

    const uint8_t *tail = (const uint8_t*)(data+nblocks*4);

    k1 = 0;

    switch (len&3) {
        case 3: k1 ^= tail[2] << 16;
        case 2: k1 ^= tail[1] << 8;
        case 1: k1 ^= tail[0];
                k1 ^= c1; k1 = rotl32(k1, 15); k1 *= c2; h1 ^= k1;
    }

    h1 ^= len;
    h1 = fmix32(h1);

    return h1;
}

uint32_t utl_murmur3_string_hash(const void *str) {
    return utl_murmur3_hash(str, strlen(str), HASH_SEED);
}

uint32_t utl_murmur3_uint32_t_hash(const void *uint32) {
    return utl_murmur3_hash(uint32, sizeof(uint32_t), HASH_SEED);
}
