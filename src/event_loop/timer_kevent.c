#include "utl/common.h"
#include "utl/event_loop/loop.h"
#include <sys/event.h>
#include <utl/event_loop/timer.h>
#include <stdint.h>
#include <stdlib.h>

// NOTE: This may come to bite me in the butt at some point in the future (or someone else).
// But we don't guard this to make sure that when it overflows that retreaded values can be used.
static uint32_t ident = 0;

static bool destroy(struct utl_event *event) {
    struct utl_event_timer *timer = utl_event_timer_try_from_event(event);
    free(timer);
    return true;
}

static bool dispatch(struct utl_event *event, const union utl_event_data data) {
    struct utl_event_timer *timer = utl_event_timer_try_from_event(event);
    timer->func(event, timer->event.user_data);
    return true;
}

static const struct utl_event_interface interface = {
    .destroy = destroy,
    .dispatch = dispatch,
};

struct utl_event *utl_event_loop_add_timer(struct utl_event_loop *loop, utl_event_timer_func event_func, void *data) {
    if (!loop || !event_func) return false;

    struct utl_event_timer *timer = calloc(1, sizeof(*timer));
    if (!timer) return NULL;

    timer->func = event_func;
    timer->event.loop = loop;
    timer->event.interface = &interface;
    timer->event.type = UTL_EVENT_TYPE_KEVENT;
    timer->event.data.kevents.arr = &timer->kevent;
    timer->event.data.kevents.size = 1;
    timer->event.user_data = data;

    EV_SET(&timer->kevent, ident, EVFILT_TIMER, EV_DISABLE, NOTE_MSECONDS, 0, &timer->event);
    ident++;

    if (!utl_event_loop_add_event(loop, &timer->event)) {
        free(timer);
        return NULL;
    }

    return &timer->event;
}

bool utl_event_timer_update(struct utl_event *event, uint32_t msecs) {
    struct utl_event_timer *timer = utl_event_timer_try_from_event(event);
    if (!timer) return false;

    timer->kevent.flags = msecs ? EV_ENABLE | EV_ONESHOT : EV_DISABLE;
    timer->kevent.data = msecs;

    return utl_event_loop_update_event(event->loop, event);
}

struct utl_event_timer *utl_event_timer_try_from_event(struct utl_event *event) {
    if (!event || event->interface != &interface) return NULL;

    struct utl_event_timer *timer = utl_container_of(event, timer, event);
    return timer;
}
