#include <utl/event_loop/timer.h>
#include <utl/common.h>
#include <sys/timerfd.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include "util.h"
#include "utl/event_loop/loop.h"

static bool timer_dispatch(struct utl_event *event, const union utl_event_data data) {
    struct utl_event_timer *timer_event = utl_event_timer_try_from_event(event);
    timer_event->func(event, event->user_data);

    return true;
}

static bool timer_destroy(struct utl_event *event) {
    struct utl_event_timer *timer_event = utl_event_timer_try_from_event(event);
    utl_event_timer_update(event, 0);
    close(timer_event->event.data.fd.fd);
    free(timer_event);

    return true;
}

static bool timer_set(struct utl_event_timer *timer_event, struct timespec timespec) {
    struct itimerspec its;
    its.it_interval.tv_sec = 0;
    its.it_interval.tv_nsec = 0;
    its.it_value = timespec;

    return (timerfd_settime(timer_event->event.data.fd.fd, 0, &its, NULL) != -1);
}

static const struct utl_event_interface interface = {
    .destroy = timer_destroy,
    .dispatch = timer_dispatch,
};

struct utl_event *utl_event_loop_add_timer(struct utl_event_loop *loop, utl_event_timer_func event_func, void *data) {
    if (!loop || !event_func) return NULL;

    struct utl_event_timer *timer_event = calloc(1, sizeof(*timer_event));
    if (!timer_event) return NULL;

    int fd = timerfd_create(CLOCK_MONOTONIC, TFD_CLOEXEC | TFD_NONBLOCK);
    if (fd < 0) {
        free(timer_event);
        return NULL;
    }

    timer_event->event.interface = &interface;
    timer_event->event.loop = loop;
    timer_event->func = event_func;
    timer_event->event.user_data = data;
    timer_event->event.type = UTL_EVENT_TYPE_FD;
    timer_event->event.data.fd.fd = fd;
    timer_event->event.data.fd.mask = UTL_EVENT_READABLE;

    if (!utl_event_loop_add_event(loop, &timer_event->event)) {
        close(timer_event->event.data.fd.fd);
        free(timer_event);
        return NULL;
    }

    return &timer_event->event;
}

bool utl_event_timer_update(struct utl_event *event, uint32_t msecs) {
    struct utl_event_timer *timer_event = utl_event_timer_try_from_event(event);
    if (!timer_event) return false;

    return timer_set(timer_event, timespec_from_milliseconds(msecs));
}

struct utl_event_timer *utl_event_timer_try_from_event(struct utl_event *event) {
    if (!event || event->interface != &interface) return NULL;

    struct utl_event_timer *timer = utl_container_of(event, timer, event);
    return timer;
}
