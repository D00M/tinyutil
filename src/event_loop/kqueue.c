#include <stdint.h>
#include <stdio.h>
#include <utl/event_loop/loop.h>
#include <utl/vector.h>
#include <utl/list.h>
#include <sys/event.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include "util.h"

#define MAX_KEVENTS 32

struct event_dispatch_state {
    struct utl_event *event;
    union {
        uint32_t mask; // For UTL_EVENT_TYPE_FD
        struct utl_vector kevents; // For UTL_EVENT_TYPE_KEVENT
    };
};

static bool event_dispatch_state_init(struct event_dispatch_state *state, struct utl_event *event) {
    memset(state, 0, sizeof(*state));
    state->event = event;
    switch (state->event->type) {
        case UTL_EVENT_TYPE_FD:
            state->mask = 0;
            break;
        case UTL_EVENT_TYPE_KEVENT:
            utl_vector_init(&state->kevents, sizeof(struct kevent));
            break;
    }

    return true;
}

static bool event_dispatch_state_deinit(struct event_dispatch_state *state) {
    switch (state->event->type) {
        case UTL_EVENT_TYPE_KEVENT:
            return utl_vector_deinit(&state->kevents);
        default:
            return true;
    }
}

static bool event_dispatch_state_add_kevent(struct event_dispatch_state *state, struct kevent *kevent) {
    if (state->event != kevent->udata) return true;

    // Mark the kevent as used and therefore skippable
    kevent->udata = NULL;

    switch (state->event->type) {
        case UTL_EVENT_TYPE_FD:
            if (kevent->filter == EVFILT_READ) state->mask |= UTL_EVENT_READABLE;
            if (kevent->filter == EVFILT_WRITE) state->mask |= UTL_EVENT_WRITEABLE;
            if (kevent->flags & EV_ERROR) state->mask |= UTL_EVENT_ERROR;
            if (kevent->flags & EV_EOF) state->mask |= UTL_EVENT_HANGUP;
            break;
        case UTL_EVENT_TYPE_KEVENT:
            utl_vector_push(&state->kevents, kevent);
            break;
    }

    return true;
}

static bool event_dispatch_state_dispatch(struct event_dispatch_state *state) {
    union utl_event_data data;
    switch (state->event->type) {
        case UTL_EVENT_TYPE_FD:
            data.fd.fd = state->event->data.fd.fd;
            data.fd.mask = state->mask;
            break;
        case UTL_EVENT_TYPE_KEVENT:
            data.kevents.arr = state->kevents.vec.data;
            data.kevents.size = utl_vector_length(&state->kevents);
            break;
    }

    return state->event->interface->dispatch(state->event, data);
}

static bool dispatch_event(struct utl_event *event, struct kevent *events, uint32_t len) {
    // All that is really needed to be known about this process is that we just
    // iterate through the event list and find kevents tied to the same event
    // and then we gather all the state amongst them and package them, then dispatch the event.
    // The technical details aren't super important but the event_dispatch_state
    // structure basically just works to dynamically make sure that each event type
    // data is gathered and dispatched correctly, in what is a remarkably simple process.
    struct event_dispatch_state state;
    event_dispatch_state_init(&state, event);

    for (int i = 0; i < len; i++) {
        struct kevent *kevent = events+i;
        if (!kevent->udata) continue;

        event_dispatch_state_add_kevent(&state, kevent);
    }

    event_dispatch_state_dispatch(&state);
    event_dispatch_state_deinit(&state);

    return true;
}

static bool fd_with_flags(int kq, struct utl_event *event, uint32_t flags) {
    struct kevent kevents[2];
    uint32_t count = 0;
    if (event->data.fd.mask & UTL_EVENT_READABLE) {
        EV_SET(&kevents[count], event->data.fd.fd, EVFILT_READ, flags, 0, 0, event);
        count++;
    }
    if (event->data.fd.mask & UTL_EVENT_WRITEABLE) {
        EV_SET(&kevents[count], event->data.fd.fd, EVFILT_WRITE, flags, 0, 0, event);
        count++;
    }
    if (!count) return false;

    return (kevent(kq, kevents, count, NULL, 0, NULL) != -1);
}

static bool kevent_with_flags(int kq, struct utl_event *event, struct kevent *kevents, uint32_t len, uint32_t with_flags, uint32_t no_flags) {
    for (int i = 0; i < len; i++) {
        kevents[i].flags &= (~no_flags);
        kevents[i].flags |= with_flags;
        kevents[i].udata = event;
    }

    return (kevent(kq, kevents, len, NULL, 0, NULL) != -1);
}

static bool event_with_flags(struct utl_event_loop *loop, struct utl_event *event, uint32_t flags, uint32_t no_flags) {
    switch (event->type) {
        case UTL_EVENT_TYPE_FD:
            return fd_with_flags(loop->fd, event, flags);
        case UTL_EVENT_TYPE_KEVENT:
            return kevent_with_flags(loop->fd, event, event->data.kevents.arr, event->data.kevents.size, flags, no_flags);
        default:
            return false;
    }
}

bool utl_event_loop_init(struct utl_event_loop *loop) {
    if (!loop) return false;

#ifdef __FreeBSD__
    loop->fd = kqueuex(KQUEUE_CLOEXEC);
#endif /* ifdef __FreeBSD__ */

#if defined(__NetBSD__) || defined(__OpenBSD__)
    loop->fd = kqueue1(O_CLOEXEC);
#endif /* ifdef __NetBSD__ || __OpenBSD__ */

    if (loop->fd == -1) return false;

    utl_list_init(&loop->events);

    return true;
}

bool utl_event_loop_deinit(struct utl_event_loop *loop) {
    if (!loop) return false;

    struct utl_event *event, *tmp;
    utl_list_for_each_safe(event, tmp, &loop->events, link) {
        utl_event_remove(event);
    }

    close(loop->fd);

    return true;
}

bool utl_event_loop_add_event(struct utl_event_loop *loop, struct utl_event *event) {
    return loop && event && event_with_flags(loop, event, EV_ADD, EV_DELETE) && utl_list_insert(&loop->events, &event->link);
}

bool utl_event_loop_update_event(struct utl_event_loop *loop, struct utl_event *event) {
    return loop && event && event_with_flags(loop, event, 0, 0);
}

bool utl_event_loop_remove_event(struct utl_event_loop *loop, struct utl_event *event) {
    return loop && event && utl_list_remove(&event->link) && event_with_flags(loop, event, EV_DELETE, EV_ADD);
}

int32_t utl_event_loop_wait(struct utl_event_loop *loop, int32_t timeout) {
    if (!loop) return -1;

    const struct timespec timeoutspec = timespec_from_milliseconds(timeout);
    struct kevent events[MAX_KEVENTS];
    int32_t count;
    while (true) {
        count = kevent(loop->fd, NULL, 0, events, MAX_KEVENTS, timeout == -1 ? NULL : &timeoutspec);
        if (count >= 0) break;
        if (errno != EINTR) return -1;
    }

    // PERF: I don't like the performance of this but I'm not entirely sure how else
    // to handle this without getting super complicated.
    for (int i = 0; i < count; i++) {
        struct kevent *kevent = events+i;
        if (!kevent->udata) continue; // If a kevent's user data is NULL we skip it.

        struct utl_event *event = kevent->udata;
        dispatch_event(event, events+i, count-i);
    }

    return count;
}
