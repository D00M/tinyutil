#include "utl/event_loop/loop.h"
#include <utl/event_loop/signal.h>
#include <utl/common.h>
#include <sys/signalfd.h>
#include <stdio.h>
#include <signal.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>

static bool signal_destroy(struct utl_event *event) {
    struct utl_event_signal *signal_event = utl_event_signal_try_from_event(event);

    sigset_t mask;
    sigemptyset(&mask);
    sigaddset(&mask, signal_event->signal);
    sigprocmask(SIG_UNBLOCK, &mask, NULL);

    close(signal_event->event.data.fd.fd);
    free(signal_event);

    return true;
}

static bool signal_dispatch(struct utl_event *event, const union utl_event_data data) {
    struct utl_event_signal *signal_event = utl_event_signal_try_from_event(event);

    struct signalfd_siginfo info;
    read(event->data.fd.fd, &info, sizeof(info));

    signal_event->func(event, signal_event->signal, signal_event->event.user_data);

    return true;
}

static const struct utl_event_interface interface = {
    .destroy = signal_destroy,
    .dispatch = signal_dispatch,
};

struct utl_event *utl_event_loop_add_signal(struct utl_event_loop *loop, utl_event_signal_func event_func, int signal, void *data) {
    if (!loop || !event_func) return NULL;

    sigset_t mask;
    sigemptyset(&mask);
    if (sigaddset(&mask, signal) == -1) return NULL;

    struct utl_event_signal *signal_event = calloc(1, sizeof(*signal_event));
    if (!signal_event) return NULL;

    int32_t fd = signalfd(-1, &mask, SFD_NONBLOCK | SFD_CLOEXEC);
    if (fd == -1) {
        free(signal_event);
        return NULL;
    }

    sigprocmask(SIG_BLOCK, &mask, NULL);

    signal_event->signal = signal;
    signal_event->func = event_func;
    signal_event->event.loop = loop;
    signal_event->event.user_data = data;
    signal_event->event.interface = &interface;
    signal_event->event.type = UTL_EVENT_TYPE_FD;
    signal_event->event.data.fd.fd = fd;
    signal_event->event.data.fd.mask = UTL_EVENT_READABLE;

    if (!utl_event_loop_add_event(loop, &signal_event->event)) {
        close(fd);
        free(signal_event);
        return NULL;
    }

    return &signal_event->event;
}

bool utl_event_signal_update(struct utl_event *event, int signal) {
    if (!event) return false;

    sigset_t mask;
    sigemptyset(&mask);
    if (sigaddset(&mask, signal) == -1) return false;

    struct utl_event_signal *signal_event = utl_event_signal_try_from_event(event);
    int fd = signalfd(signal_event->event.data.fd.fd, &mask, SFD_NONBLOCK | SFD_CLOEXEC);
    if (signal_event->event.data.fd.fd == -1) return false;

    // Unblock old signal then block new signal
    sigprocmask(SIG_BLOCK, &mask, NULL);
    sigdelset(&mask, signal);
    sigaddset(&mask, signal_event->signal);
    sigprocmask(SIG_UNBLOCK, &mask, NULL);

    signal_event->event.data.fd.fd = fd;
    signal_event->signal = signal;

    return true;
}

struct utl_event_signal *utl_event_signal_try_from_event(struct utl_event *event) {
    if (!event || event->interface != &interface) return NULL;

    struct utl_event_signal *signal = utl_container_of(event, signal, event);
    return signal;
}
