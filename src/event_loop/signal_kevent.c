#include "utl/event_loop/loop.h"
#include <utl/event_loop/signal.h>
#include <signal.h>
#include <stdlib.h>
#include <sys/event.h>

static bool destroy(struct utl_event *event) {
    struct utl_event_signal *signal = utl_event_signal_try_from_event(event);

    sigset_t mask;
    sigemptyset(&mask);
    sigaddset(&mask, signal->signal);
    sigprocmask(SIG_UNBLOCK, &mask, NULL);

    free(signal);
    return true;
}

static bool dispatch(struct utl_event *event, const union utl_event_data data) {
    struct utl_event_signal *signal = utl_event_signal_try_from_event(event);
    signal->func(event, signal->signal, signal->event.user_data);
    return true;
}

static const struct utl_event_interface interface = {
    .destroy = destroy,
    .dispatch = dispatch,
};

struct utl_event *utl_event_loop_add_signal(struct utl_event_loop *loop, utl_event_signal_func event_func, int signal, void *data) {
    if (!loop || !event_func) return NULL;

    sigset_t mask;
    sigemptyset(&mask);
    if (sigaddset(&mask, signal) == -1) return false;

    struct utl_event_signal *event = calloc(1, sizeof(*event));
    event->signal = signal;
    event->func = event_func;
    event->event.loop = loop;
    event->event.interface = &interface;
    event->event.user_data = data;
    event->event.type = UTL_EVENT_TYPE_KEVENT;
    event->event.data.kevents.arr = &event->kevent;
    event->event.data.kevents.size = 1;

    EV_SET(&event->kevent, signal, EVFILT_SIGNAL, 0, 0, 0, &event->event);

    if (!utl_event_loop_add_event(loop, &event->event)) {
        free(event);
        return NULL;
    }

    sigprocmask(SIG_BLOCK, &mask, NULL);

    return &event->event;
}

bool utl_event_signal_update(struct utl_event *event, int signal) {
    struct utl_event_signal *signal_event = utl_event_signal_try_from_event(event);
    if (!signal_event) return false;

    sigset_t mask;
    sigemptyset(&mask);
    if (sigaddset(&mask, signal) == -1) return false;

    // Unblock old signal then block new signal
    sigprocmask(SIG_BLOCK, &mask, NULL);
    sigdelset(&mask, signal);
    sigaddset(&mask, signal_event->signal);
    sigprocmask(SIG_UNBLOCK, &mask, NULL);

    // Remove the old signal kevent then add the new one.
    struct kevent new_signal[2];
    new_signal[0] = signal_event->kevent;
    new_signal[0].flags = EV_DELETE;
    EV_SET(&new_signal[1], signal, EVFILT_SIGNAL, EV_ADD, 0, 0, event);

    event->data.kevents.arr = new_signal;
    event->data.kevents.size = 2;

    if (!utl_event_loop_update_event(event->loop, event)) return false;

    signal_event->signal = signal;
    signal_event->kevent = new_signal[1];
    event->data.kevents.arr = &signal_event->kevent;
    event->data.kevents.size = 1;

    return true;
}

struct utl_event_signal *utl_event_signal_try_from_event(struct utl_event *event) {
    if (!event || event->interface != & interface) return NULL;

    struct utl_event_signal *signal = utl_container_of(event, signal, event);
    return signal;
}
