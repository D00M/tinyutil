#include "utl/list.h"
#include <utl/event_loop/loop.h>

bool utl_event_remove(struct utl_event *event) {
    return event && event->loop && utl_event_loop_remove_event(event->loop, event) && event->interface->destroy(event);
}
