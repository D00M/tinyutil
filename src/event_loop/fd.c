#include "utl/list.h"
#include <stdio.h>
#include <utl/common.h>
#include <utl/event_loop/loop.h>
#include <utl/event_loop/fd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>

static bool fd_destroy(struct utl_event *utl_event) {
    if (!utl_event) return false;

    struct utl_event_fd *event = utl_event_fd_try_from_event(utl_event);
    close(event->event.data.fd.fd);
    free(event);

    return true;
}

static bool fd_dispatch(struct utl_event *utl_event, const union utl_event_data data) {
    if (!utl_event) return false;

    struct utl_event_fd *event = utl_event_fd_try_from_event(utl_event);
    event->func(utl_event, event->fd, data.fd.mask, event->event.user_data);

    return true;
}

static const struct utl_event_interface interface = {
    .destroy = fd_destroy,
    .dispatch = fd_dispatch,
};

struct utl_event *utl_event_loop_add_fd(struct utl_event_loop *loop, utl_event_fd_func event_func, int fd, uint32_t mask, void *data) {
    if (!loop || !event_func || !(mask & UTL_EVENT_READABLE || mask & UTL_EVENT_WRITEABLE)) return NULL;

    struct utl_event_fd *event = calloc(1, sizeof(*event));
    if (!event) return NULL;

    int dup = fcntl(fd, F_DUPFD_CLOEXEC, 0);
    if (dup == -1) {
        free(event);
        return NULL;
    }

    event->fd = fd;
    event->func = event_func;
    event->event.interface = &interface;
    event->event.loop = loop;
    event->event.user_data = data;
    event->event.type = UTL_EVENT_TYPE_FD;
    event->event.data.fd.fd = dup;
    event->event.data.fd.mask = mask;

    if (!utl_event_loop_add_event(loop, &event->event)) {
        close(event->event.data.fd.fd);
        free(event);
        return NULL;
    }

    return &event->event;
}

bool utl_event_fd_update(struct utl_event *utl_event, uint32_t mask) {
    if (!utl_event || !utl_event->loop || !(mask & UTL_EVENT_READABLE || mask & UTL_EVENT_WRITEABLE)) return false;

    struct utl_event_fd *event = utl_event_fd_try_from_event(utl_event);
    if (!event) return false;

    event->event.data.fd.mask = mask;
    return utl_event_loop_update_event(event->event.loop, utl_event);
}

struct utl_event_fd *utl_event_fd_try_from_event(struct utl_event *event) {
    if (!event || event->interface != &interface) return NULL;

    struct utl_event_fd *fd = utl_container_of(event, fd, event);
    return fd;
}

