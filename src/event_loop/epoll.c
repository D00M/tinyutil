#include <errno.h>
#include <stdint.h>
#include <utl/list.h>
#include <utl/event_loop/loop.h>
#include <sys/epoll.h>
#include <unistd.h>

#define MAX_EVENTS 32

bool utl_event_loop_init(struct utl_event_loop *loop) {
    if (!loop) return false;

    loop->fd = epoll_create1(EPOLL_CLOEXEC);
    if (loop->fd == -1) return false;

    utl_list_init(&loop->events);

    return true;
}

bool utl_event_loop_deinit(struct utl_event_loop *loop) {
    if (!loop) return false;

    struct utl_event *event, *tmp;
    utl_list_for_each_safe(event, tmp, &loop->events, link) {
        utl_event_remove(event);
    }

    close(loop->fd);

    return true;
}

bool utl_event_loop_add_event(struct utl_event_loop *loop, struct utl_event *event) {
    if (!loop || !event) return false;

    uint32_t epoll_mask = 0;
    if (event->data.fd.mask & UTL_EVENT_READABLE) epoll_mask |= EPOLLIN;
    if (event->data.fd.mask & UTL_EVENT_WRITEABLE) epoll_mask |= EPOLLOUT;
    if (!epoll_mask) return false;

    struct epoll_event epoll_event;
    epoll_event.events = epoll_mask;
    epoll_event.data.ptr = event;

    if (epoll_ctl(loop->fd, EPOLL_CTL_ADD, event->data.fd.fd, &epoll_event) == -1) {
        return false;
    }

    utl_list_insert(&loop->events, &event->link);

    return true;
}

bool utl_event_loop_update_event(struct utl_event_loop *loop, struct utl_event *event) {
    if (!loop || !event) return false;

    uint32_t epoll_mask = 0;
    if (event->data.fd.mask & UTL_EVENT_READABLE) epoll_mask |= EPOLLIN;
    if (event->data.fd.mask & UTL_EVENT_WRITEABLE) epoll_mask |= EPOLLOUT;

    struct epoll_event epoll_event;
    epoll_event.events = epoll_mask;
    epoll_event.data.ptr = event;

    int32_t ret = epoll_ctl(loop->fd, EPOLL_CTL_MOD, event->data.fd.fd, &epoll_event);

    return (ret != -1);
}

bool utl_event_loop_remove_event(struct utl_event_loop *loop, struct utl_event *event) {
    if (!loop || !event) return false;

    utl_list_remove(&event->link);

    struct epoll_event epoll_event;
    epoll_event.data.ptr = NULL;
    epoll_event.events = 0;

    // NOTE: The goal of this call is to hopefully change the data in the event before deletion
    // so we don't have to store the deleted events before destruction.
    // Instead the epoll event will be raised instead with a NULL data and then can easily be ignored.
    epoll_ctl(loop->fd, EPOLL_CTL_MOD, event->data.fd.fd, &epoll_event);

    int32_t ret = epoll_ctl(loop->fd, EPOLL_CTL_DEL, event->data.fd.fd, NULL);

    return (ret != -1);
}

int32_t utl_event_loop_wait(struct utl_event_loop *loop, int32_t timeout) {
    if (!loop) return -1;

    struct epoll_event events[MAX_EVENTS];
    int32_t count;
    while (true) {
        count = epoll_wait(loop->fd, events, MAX_EVENTS, timeout);
        if (count >= 0) break;
        if (errno != EINTR) return false;
    }

    for (int i = 0; i < count; i++) {
        struct epoll_event *epoll_event = events+i;
        if (!epoll_event->data.ptr) continue;

        uint32_t mask = 0;
        if (epoll_event->events & EPOLLIN)  mask |= UTL_EVENT_READABLE;
        if (epoll_event->events & EPOLLOUT) mask |= UTL_EVENT_WRITEABLE;
        if (epoll_event->events & EPOLLERR) mask |= UTL_EVENT_ERROR;
        if (epoll_event->events & EPOLLHUP) mask |= UTL_EVENT_HANGUP;

        struct utl_event *event = epoll_event->data.ptr;
        event->interface->dispatch(event, (union utl_event_data){
                .fd.fd = event->data.fd.fd,
                .fd.mask = mask,
                });
    }

    return count;
}
