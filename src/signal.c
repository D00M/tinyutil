#include <utl/common.h>
#include <utl/list.h>
#include <utl/signal.h>
#include <stdint.h>
#include <string.h>

static bool signal_list_iterator(struct utl_list *link, void *data);

bool signal_list_iterator(struct utl_list *link, void *data) {
    struct utl_listener *listener = utl_container_of(link, listener, link);
    listener->notify(listener, data);
    return false;
}

bool utl_signal_init(struct utl_signal *signal) {
    return signal && utl_list_init(&signal->listeners);
}

bool utl_signal_add(struct utl_signal *signal, struct utl_listener *listener) {
    return signal && listener && listener->notify && utl_list_insert(&signal->listeners, &listener->link);
}

struct utl_listener *utl_signal_get(struct utl_signal *signal, utl_listener_func notify) {
    if (!signal || !notify) return NULL;
    struct utl_listener *listener;
    utl_list_for_each(listener, &signal->listeners, link) if (notify == listener->notify) return listener;
    return NULL;
}

bool utl_signal_emit(struct utl_signal *signal, void *data) {
    return signal && utl_list_iterate_mutable(&signal->listeners, signal_list_iterator, data);
}
