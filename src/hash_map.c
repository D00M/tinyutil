#include <utl/hash_map.h>

bool utl_hash_map_init(struct utl_hash_map *map, utl_hash_func hash) {
    if (!map || !hash) return false;

    map->hash = hash;
    return utl_hash_table_init(&map->table);
}

bool utl_hash_map_deinit(struct utl_hash_map *map) {
    if (!map) return false;

    return utl_hash_table_deinit(&map->table);
}

bool utl_hash_map_elements_deinit(struct utl_hash_map *map, utl_destroy_func destroy) {
    if (!map) return false;

    return utl_hash_table_elements_deinit(&map->table, destroy);
}

bool utl_hash_map_insert(struct utl_hash_map *map, const void *key, void *value) {
    if (!map || !key || !value) return false;

    return utl_hash_table_insert(&map->table, map->hash(key), value);
}

void *utl_hash_map_remove(struct utl_hash_map *map, const void *key) {
    if (!map || !key) return NULL;

    return utl_hash_table_remove(&map->table, map->hash(key));
}

void *utl_hash_map_get(const struct utl_hash_map *map, const void *key) {
    if (!map || !key) return NULL;

    return utl_hash_table_get(&map->table, map->hash(key));
}

bool utl_hash_map_iterate(const struct utl_hash_map *map, void *user_data, utl_iterator_func iterate) {
    if (!map || !iterate) return false;

    struct utl_hash_table_entry *entry;
    for (int i = 0; i < map->table.allocated; i++) {
        entry = map->table.entries + i;

        if (!utl_hash_table_entry_is_filled(entry) || utl_hash_table_entry_is_deleted(entry)) continue;
        if (iterate(entry->data, user_data)) break;
    }

    return true;
}
