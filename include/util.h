#ifndef UTIL_H_
#define UTIL_H_

#include <stdint.h>
#include <time.h>

#define CMP_SMALLER(cmp) ((cmp) < 0)
#define CMP_LARGER(cmp) ((cmp) > 0)
#define CMP_EQUAL(cmp) ((cmp) == 0)
#define STRING_EQUAL(str1, str2) CMP_EQUAL(strcmp((str1), (str2)))
#define STRINGN_EQUAL(str1, str2, n) CMP_EQUAL(strncmp((str1), (str2), (n)))
#define ARRAY_LENGTH(arr) (sizeof(arr) / sizeof(arr[0]))
#define BITMASK_ZERO_BIT(mask, bit) (mask &= ~(bit))

struct timespec timespec_from_milliseconds(uint32_t msecs);

#endif // UTIL_H_
