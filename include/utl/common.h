#ifndef UTL_COMMON_H_
#define UTL_COMMON_H_

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

#define utl_container_of(ptr, sample, member) \
    (__typeof__(sample))((char*)(ptr) - offsetof(__typeof__(*sample), member))

typedef uint32_t (*utl_hash_func)(const void *data);
typedef int64_t (*utl_cmp_func)(const void *data1, const void *data2);
typedef void (*utl_destroy_func)(void *data);

/// If true is returned the iterating object, can break from the loop.
/// So if the user wishes to iterate through all of the object's data,
/// then return false always.
typedef bool (*utl_iterator_func)(const void *data, void *user_data);

uint32_t utl_murmur3_hash(const void *key, size_t len, uint32_t seed);
uint32_t utl_murmur3_string_hash(const void *str);
uint32_t utl_murmur3_uint32_t_hash(const void *uint32);

#endif // UTL_COMMON_H_
