#ifndef UTL_ORDERED_VECTOR_H_
#define UTL_ORDERED_VECTOR_H_

#include <utl/vector.h>

/** @interface utl_vector_sort_interface
 *
 * @brief The custom sorting interface for the utl_ordered_vector.
 *
 * The point of having this interface is to allow the user to use some custom
 * sorting and searching algorithms.
 *
 * @memberof utl_ordered_vector
 */
struct utl_vector_sort_interface {
    int32_t (*search)(const struct utl_vector *vector, utl_cmp_func cmp, const void *data);
    void (*sort)(struct utl_vector *vector, utl_cmp_func cmp);

    /**
     * If utilized an index can provide a speed up that allows us to focus on an index we know is different.
     * For example for insertion sort if the user inserts data into index 5 we just sort that element.
     * Do note that you should only provide an index if you know the rest of the vector is sorted.
     *
     * @return If `index` is not -1 then return the new index of the element. -1 on error.
     */
    int32_t (*sort_insert)(struct utl_vector *vector, utl_cmp_func cmp, int32_t index);
};

/** @struct utl_ordered_vector
 *
 * @brief An automatically sorted vector.
 *
 * @note Thanks to kolunmi for the name.
 *
 * This vector stays automatically sorted as long as it's interface is used.
 * The default utl_vector_sort_interface uses quick sort, insertion sort, and
 * binary search. The user may reach into the vector and perform manually operations.
 * But, by doing so the user cannot guarantee that the vector has maintained order.
 * The user should call utl_ordered_vector_sort() after any manual operations on the underlying vector.
 *
 * @sa utl_vector
 */
struct utl_ordered_vector {
    struct utl_vector vector; //!< underlying vector
    const struct utl_vector_sort_interface *interface; //!< active sorting interface
    utl_cmp_func cmp; //!< data compare function
};

/** Initialize an ordered vector.
 *
 * @param vector The vector to initialize.
 * @param cmp The function used to compare elements in `vector`.
 * @parma element_size Size in bytes of each element.
 *
 * @return True if initialization was successful, otherwise false if `vector` or `cmp` was NULL or utl_vector initialization failed.
 *
 * @see utl_vector_init()
 *
 * @memberof utl_ordered_vector
 */
bool utl_ordered_vector_init(struct utl_ordered_vector *vector, utl_cmp_func cmp, size_t element_size);

/** Deinitialize an ordered vector.
 *
 * @note This function only deinitializes the vector itself,
 * it doesn't not touch the underlying elements.
 * If the desire is to handle the destruction of the elements the user should
 * use utl_vector_elements_deinit().
 *
 * @param vector Vector to deinitialize.
 *
 * @return True if deinitialization was successful, otherwise false if `vector` was NULL.
 *
 * @memberof utl_ordered_vector
 */
bool utl_ordered_vector_deinit(struct utl_ordered_vector *vector);

/** Deinitialize an ordered vector and handle it's elements.
 *
 * @param vector Vector to deinitialize.
 * @param destroy Function to call on each element.
 *
 * @return True if destruction was successful, otherwise false if `vector` or `destroy` was NULL.
 *
 * @memberof utl_ordered_vector
 */
bool utl_ordered_vector_elements_deinit(struct utl_ordered_vector *vector, utl_destroy_func destroy);

/** Attach a sorting interface to an ordered vector.
 *
 * @param vector The vector to attach `interface` to.
 * @param interface The interface to attach to `vector`.
 *
 * @return True if interface attachment was successful, otherwise false if `vector` or `interface` was NULL.
 *
 * @memberof utl_ordered_vector
 */
bool utl_ordered_vector_attach_interface(struct utl_ordered_vector *vector, const struct utl_vector_sort_interface *interface);

/** Insert data into a new element in an ordered vector.
 *
 * @param vector The vector to insert into.
 * @param data The new data to copy into `vector`.
 *
 * @return A pointer to the new element, otherwise NULL if `vector` was NULL, utl_vector_push() failed or `vector.interface.sort_insert` was NULL.
 *
 * @see utl_vector_push()
 *
 * @memberof utl_ordered_vector
 */
void *utl_ordered_vector_push(struct utl_ordered_vector *vector, const void *data);

/** Get an element from an ordered vector.
 *
 * @param vector The vector to get the element from.
 * @param index The index of an element in `vector`. The valid index range is `[0, vector.length)`.
 *
 * @return A pointer into `vector` at index, otherwise NULL if the index was too large or `vector` was NULL.
 *
 * @memberof utl_ordered_vector
 */
void *utl_ordered_vector_get(const struct utl_ordered_vector *vector, uint32_t index);

/** Remove an element from an ordered vector.
 *
 * @param vector The vector to get data from.
 * @param index The index in `vector`. The valid index range is `[0, vector.length)`.
 *
 * @return True if removal was successful, otherwise false if `vector` was NULL or `index` was too large.
 *
 * @memberof utl_ordered_vector
 */
bool utl_ordered_vector_remove(struct utl_ordered_vector *vector, uint32_t index);

/** An implementation defined search of an ordered vector.
 *
 * @param vector The vector to search through.
 * @param data The data to search for in `vector`.
 *
 * @return The index of `data` in the vector, otherwise -1 if `data` wasn't found or `vector` or `vector.interface.search` was NULL.
 *
 * @memberof utl_ordered_vector
 */
int32_t utl_ordered_vector_search(const struct utl_ordered_vector *vector, const void *data);

/** Perform an implementation defined sort of an ordered vector.
 *
 * @param vector The vector to sort.
 *
 * @return True if the sorting was successful, otherwise false if `vector` or `vector.interface.sort` was NULL.
 *
 * @memberof utl_ordered_vector
 */
bool utl_ordered_vector_sort(struct utl_ordered_vector *vector);

/** A quick sort implementation for a utl_vector.
 *
 * @warning The function may not execute if `vector` or `cmp` is NULL.
 *
 * @param vector The vector to sort.
 * @param cmp The compare function to use on each element.
 */
void utl_vector_quick_sort(struct utl_vector *vector, utl_cmp_func cmp);

/** An insertion sort ipmlementation for a utl_vector.
 *
 * @param vector The vector to sort.
 * @param cmp The compare function to use on each element.
 * @param index The index of the element to sort in `vector`. This can be -1 if the caller wants to sort the entirety `vector`. The valid index range is `[-1, vector.length)`.
 *
 * @return The new index of the element that was at `index`, otherwise -1 if `index` was -1 or `cmp` or `vector` was NULL or `index` was too large.
 */
int32_t utl_vector_insertion_sort(struct utl_vector *vector, utl_cmp_func cmp, int32_t index);

/** A binary search implementation for a utl_vector.
 *
 * @warning This function will fail if `vector` is not sorted.
 *
 * @param vector The vector to search.
 * @param cmp The compare function to use on each element.
 * @param data The data to compare with each element using `cmp`.
 *
 * @return The index in `vector` where `data` lives, otherwise -1 if `data` wasn't found or `vector` or `cmp` was NULL.
 */
int32_t utl_vector_binary_search(const struct utl_vector *vector, utl_cmp_func cmp, const void *data);

#endif  // UTL_ORDERED_VECTOR_H_
