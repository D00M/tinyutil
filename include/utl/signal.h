#ifndef UTL_SIGNAL_H_
#define UTL_SIGNAL_H_

#include <utl/list.h>

/** @page signal_listener Signals and Listeners
 *
 * Both the utl_signal and utl_listener represent a generic
 * implementation of the Observer pattern. Wherein a utl_signal
 * can emit a signal to multiple listeners.
 *
 * This can be powerfully applied to allow for different parts of a system to
 * respond to events or state changes. All while remaining agnostic of the different 
 * types that might listen in on these events.
 *
 * For example given a utl_signal `event` and the some listeners `l1` and `l2`:
 * ```c
 * struct utl_signal event;
 * struct utl_listener l1, l2;
 *
 * utl_signal_init(&event);
 *
 * l1.notify = callback1;
 * l2.notify = callback2;
 *
 * utl_signal_add(&event, &l1);
 * utl_signal_add(&event, &l2);
 * ```
 *
 * The utl_signal may also be apart of a structure, let's call this structure `foo`.
 * ```c
 * utl_signal_add(&foo.event, &l1);
 * utl_signal_add(&foo.event, &l2);
 * ```
 *
 * Upon `utl_signal_emit()` being called on `event`, `callback1` and `callback2` will be called.
 *
 * If the listener being called is a field in a structure the structure can be retrieved with utl_container_of():
 * ```c
 * void changes(struct utl_listener *listener, void *data) {
 *  struct foo *foo = utl_container_of(listener, foo, field_name);
 *  ...
 * }
 * ```
 * 
 * @sa utl_signal
 * @sa utl_listener
 * @sa utl_container_of
 */

struct utl_signal;
struct utl_listener;

typedef void (*utl_listener_func)(struct utl_listener *listener, void *data);

/** @struct utl_signal
 *
 * @brief An emittable signal.
 *
 * @sa utl_listener
 * @sa @ref signal_listener
 */
struct utl_signal {
    /// The listeners of the signal.
    struct utl_list listeners; // struct utl_listener
};

/** @struct utl_listener
 *
 * @brief A single listener of a signal.
 *
 * @sa utl_signal
 * @sa @ref signal_listener
 */
struct utl_listener {
    utl_listener_func notify; //!< The callback to run when a signal is emitted.
    struct utl_list link; // utl_signal.listeners
};

/** Initialize a signal.
 *
 * @param signal The signal to initialize.
 *
 * @return True if successful, otherwise false if `signal` was NULL.
 *
 * @memberof utl_signal
 */
bool utl_signal_init(struct utl_signal *signal);

/** Add a listener to a signal.
 *
 * @warning This function does not protect against adding a listener twice. Adding a listener twice will cause list corruption.
 *
 * @param signal The signal to listen to.
 * @param listener The listener to add to `signal`.
 *
 * @return True if successful, otherwise false if `signal`, `listener` or `listener.notify` was NULL.
 *
 * @memberof utl_signal
 * @memberof utl_listener
 */
bool utl_signal_add(struct utl_signal *signal, struct utl_listener *listener);

/** Get a listener based on the listeners signal.
 *
 * @note This is an O(n) operation.
 *
 * @param signal The signal to search.
 * @param notify The function to compare in each listener.
 *
 * @return A valid listener with the given notify function, otherwise NULL if no listener was found.
 *
 * @memberof utl_signal
 * @memberof utl_listener
 */
struct utl_listener *utl_signal_get(struct utl_signal *signal, utl_listener_func notify);

/** Emit the signal.
 *
 * @param signal The signal to emit.
 * @param data User data to be passed to each listener.
 *
 * @return True if iteration was successful, otherwise false if `signal` was NULL or utl_list_iterate_mutable() failed.
 *
 * @memberof utl_signal
 */
bool utl_signal_emit(struct utl_signal *signal, void *data);

#endif // UTL_SIGNAL_H_
