#ifndef UTL_LIST_H_
#define UTL_LIST_H_

#include <stdbool.h>
#include <stdint.h>
#include <utl/common.h>

struct utl_list;

/// It is assumed that if true is returned that we can stop iterating.
typedef bool (*utl_list_iterator_func)(struct utl_list *link, void *data);

/** Iterate through a list.
 *
 * @warning These macros do not protect against removals during iteration 
 * see utl_list_for_each_safe() and utl_list_iterate_mutable().
 *
 * @param pos The variable which will store the current element in the list.
 * @param list The list head. @warning If `list` is not the head then this iterator will collide with the head of the list causing bad pointer arithmetic.
 * @param memeber The name of the field which links the elements together.
 *
 * @memberof utl_list
 */
#define utl_list_for_each(pos, list, member) \
    for ((pos) = utl_container_of((list)->next, (pos), member); \
            &(pos)->member != (list); \
            pos = utl_container_of((pos)->member.next, (pos), member))

/** Iterate through a list in reverse.
 *
 * @warning These macros do not protect against removals during iteration 
 * see utl_list_for_each_safe() and utl_list_iterate_mutable().
 *
 * @param pos The variable which will store the current element in the list.
 * @param list The list head. @warning If `list` is not the head then this iterator will collide with the head of the list causing bad pointer arithmetic.
 * @param memeber The name of the field which links the elements together.
 *
 * @memberof utl_list
 */
#define utl_list_for_each_rev(pos, list, member) \
    for ((pos) = utl_container_of((list)->prev, (pos), member); \
            &(pos)->member != (head); \
            pos = utl_container_of((pos)->member.prev, (pos), member))

/** Iterate through a list while protecting from the current element getting removed.
 *
 * This iterator protects from NULL dereference by keeping an extra element in the list.
 *
 * @warning This still doesn't protect against removals of the current element and the
 * next immediate element, see utl_list_iterate_mutable().
 *
 * @param pos The variable which will store the current element in the list.
 * @param tmp The variable which will store the next immedate element to `pos` in the list.
 * @param list The list head. @warning If `list` is not the head then this iterator will collide with the head of the list causing bad pointer arithmetic.
 * @param memeber The name of the field which links the elements together.
 * 
 * @memberof utl_list
 */
#define utl_list_for_each_safe(pos, tmp, head, member) \
    for ((pos) = utl_container_of((head)->next, (pos), member), \
            (tmp) = utl_container_of((pos)->member.next, (tmp), member);	\
            &(pos)->member != (head);	\
            (pos) = (tmp), \
            (tmp) = utl_container_of((pos)->member.next, (tmp), member))

/** Iterate, in reverse, through a list while protecting from the current element getting removed.
 *
 * This iterator protects from NULL dereference by keeping an extra element in the list.
 *
 * @warning This still doesn't protect against removals of the current element and the
 * next immediate element, see utl_list_iterate_mutable().
 *
 * @param pos The variable which will store the current element in the list.
 * @param tmp The variable which will store the next immedate element to `pos` in the list.
 * @param list The list head. @warning If `list` is not the head then this iterator will collide with the head of the list causing bad pointer arithmetic.
 * @param memeber The name of the field which links the elements together.
 * 
 * @memberof utl_list
 */
#define utl_list_for_each_rev_safe(pos, tmp, head, member) \
    for ((pos) = utl_container_of((head)->prev, (pos), member), \
            (tmp) = utl_container_of((pos)->member.prev, (tmp), member);	\
            &(pos)->member != (head);	\
            (pos) = (tmp), \
            (tmp) = utl_container_of((pos)->member.prev, (tmp), member))

/** @struct utl_list
 *
 * @brief A doubly linked list.
 *
 * The utl_list is the head and nodes with a list.
 *
 * Typically though, the utl_list alone represents the sentinel head of the list and
 * when as a field in structure, is the link between elements named link by convention.
 * For example with a singular utl_list `head` and a structure `element` with a utl_list field `link`.
 * The list is initialized by using utl_list_init() on `head` then inserting `e1`, `e2` and `e3` with utl_list_insert().
 *
 * ```c
 * struct element {
 *  ...
 *  struct utl_list link;
 * };
 *
 * struct utl_list head;
 * struct element e1, e2, e3;
 *
 * utl_list_init(&head);
 * utl_list_insert(&head, &e1.link);
 * utl_list_insert(&e1.link, &e2.link);
 * utl_list_insert(&head, &e3.link);
 * ```
 *
 * The outcome of this example is a list that looks like `[head, e3, e1, e2]`.
 * Note that despite the language in the api and 
 * the fact that we represent `head` as the head of the list,
 * there is no distinction between the head of a list and it's elements. 
 * The utl_list api, largely, does not enforce any distinction between 
 * the two and for efficiency reasons largely nd does 
 * not do much any error checking on the list itself.
 *
 * The utl_list api also provides iterator macros:
 * ```c
 * utl_list_for_each(element, &head, link) {
 *  task(element);
 * }
 * ```
 *
 * Although the utl_list api largely has no type enforcement on the list elements.
 * The iterator macros require each element to be the same type.
 *
 * @note I pretty much just took wayland-util's wl_list implementation.
 */
struct utl_list {
    struct utl_list *next, *prev;
};

/** Initialize a list.
 *
 * @param list The list to be initialized.
 *
 * @return True if initialization was successful, otherwise false if `list` was NULL.
 *
 * @memberof utl_list
 */
bool utl_list_init(struct utl_list *list);

/** Insert an element into a list.
 *
 * @param list The list head or an element within a list.
 * @param element The element to be inserted into `list`.
 *
 * @return True if insertion was successful, otherwise false if `list` or `element` was NULL.
 *
 * @memberof utl_list
 */
bool utl_list_insert(struct utl_list *list, struct utl_list *element);

/** Remove an element from a list.
 *
 * @param element The element to remove.
 *
 * @return True if removal was successful, otherwise false if `element` was NULL.
 *
 * @memberof utl_list
 */
bool utl_list_remove(struct utl_list *element);

/** Get the "index" of an element in a list.
 *
 * @note This is an O(n) operation.
 *
 * @note In this context the 0th element in the list is the element directly after `list`.
 *
 * @param list The list node to start searching after.
 * @param element The element to search for in `list`.
 *
 * @return The index of `element`, otheriwse -1 if `list` or `element` was NULL
 *
 * @memberof utl_list
 */
int32_t utl_list_find(struct utl_list *list, struct utl_list *element);

/** Get the element of a list at an "index".
 *
 * @note This is an O(n) operation.
 *
 * @note In this context the 0th element in the list is the element directly after `list`.
 *
 * @param list The list node to start iterating from.
 * @param index Index to iterate to after `list`.
 *
 * @return The element at `index` in `list`, otherwise NULL because the index was out of bounds of the list or `list` was NULL.
 *
 * @memberof utl_list
 */
struct utl_list *utl_list_at(struct utl_list *list, uint32_t index);

/** Get the length of a list.
 *
 * @note This is an O(n) operation.
 *
 * @param list The list node to start at.
 *
 * @return The length of the list, otherwise -1 if `list`, `list.next` or `list.next` was NULL.
 *
 * @memberof utl_list
 */
int32_t utl_list_length(struct utl_list *list);

/** Check if a list is empty.
 *
 * @param list The list node to check for emptiness.
 *
 * @return True if the list empty otherwise false if not empty or `list` was NULL.
 *
 * @memberof utl_list
 */
bool utl_list_empty(struct utl_list *list);

/** Insert one list into another.
 *
 * @warning `other` being in `list` will lead to list corruption.
 * This function will also leave `other` in an invalid state.
 *
 * @param list The list node to insert into.
 * @param other The other list node to insert.
 *
 * @return True if `other` was successfully inserted into `list` or `other` was empty, otherwise false if `list` or `other` was NULL.
 *
 * @memberof utl_list
 */
bool utl_list_insert_list(struct utl_list *list, struct utl_list *other);

/** Mutably iterate over a list.
 *
 * The utl_list safe iterator macros only allow for at most the current
 * element to be removed during iteration. This function allows the user to modify the
 * entire list during iteration. Meaning not only can the current element be removed
 * from the list but multiple elements beyond.
 *
 * @warning This does not mean the user can simply start iterating through the list themselves
 * and remove elements, this function relies on two elements inserted into the list by the function.
 * Removing either of these elements during iteration will cause undefined behavior.
 *
 * Given the list `[ head -> element1 -> element2 -> head ]`,
 * this iterator function mainly solves this problem:
 * During iteration the parent structure of `element1` signals to the parent structure of `element2`.
 * This signal causes parent of `element2` to remove itself from the list.
 * Normally with the safe iterator macros this would cause a null dereference.
 * But since the iterator function uses it's own elements for keeping track of where it is and where to stop,
 * the user may remove any elements they own and not run into problems.
 *
 * @param list The list node to start iterating at.
 * @param iterator_func The callback to run upon each iteration.
 * @param data User data to provide in `iterator_func` each iteration.
 *
 * @return True if iteration was successful, otherwise false if `list` or `iterator_func` was NULL.
 *
 * @memberof utl_list
 */
bool utl_list_iterate_mutable(struct utl_list *list, utl_list_iterator_func iterator_func, void *data);

#endif // UTL_LIST_H_
