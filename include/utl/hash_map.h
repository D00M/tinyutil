#ifndef UTL_HASH_MAP_H_
#define UTL_HASH_MAP_H_

#include <utl/common.h>
#include <utl/hash_table.h>

/** @struct utl_hash_map
 *
 * @brief A hash map.
 *
 * @sa utl_hash_table
 */
struct utl_hash_map {
    struct utl_hash_table table;
    utl_hash_func hash;
};

/** Initialize a hash map.
 *
 * @param map The hash map to initialize.
 * @param hash The function to use to hash new data.
 *
 * @return True if the initialization was successful, otherwise false if `map` or `hash` was NULL.
 *
 * @memberof utl_hash_map
 */
bool utl_hash_map_init(struct utl_hash_map *map, utl_hash_func hash);

/** Deinitialize a hash map
 *
 * @note This function only deinitializes the hash map,
 * if the user wishes to act on the entries before deinitialization
 * they should use utl_hash_map_elements_deinit().
 *
 * @param map The hash map to deinitialize.
 *
 * @return True if deinitialization was successful, otherwise false if `map` was NULL.
 *
 * @memberof utl_hash_map
 */
bool utl_hash_map_deinit(struct utl_hash_map *map);

/** Deinitialize a hash map and handle its entries.
 *
 * @param map The hash map to destroy.
 * @param destroy The function to call on each entry of the hash map.
 *
 * @return True if destruction was successful, false if args were invalid.
 *
 * @memberof utl_hash_map
 */
bool utl_hash_map_elements_deinit(struct utl_hash_map *map, utl_destroy_func destroy);

/** Insert into a hash map.
 *
 * @param map The hash map to insert into.
 * @param key The data to hash and associate with `value`.
 * @param value the data to associate with the hash of `key`.
 *
 * @return True if `key` was successfully hashed and `value` inserted into the hash map,
 * otherwise false if the hash of `key` already existed in the underlying table, `table`, `key` or `value` was NULL, or utl_hash_table_insert() failed.
 *
 * @memberof utl_hash_map
 */
bool utl_hash_map_insert(struct utl_hash_map *map, const void *key, void *value);

/** Get data from a hash map.
 *
 * @param map The hash map to get from.
 * @param key The data to hash and query into the hash map.
 *
 * @return The pointer to the data associated with the hash of `key`, otherwise NULL if the hash of `key` wasn't found, `map` or `key` was NULL or utl_hash_table_get() failed.
 *
 * @memberof utl_hash_map
 */
void *utl_hash_map_get(const struct utl_hash_map *map, const void *key);

/** Remove an entry from a hash map.
 *
 * @param map The hash map to remove from.
 * @param key The data to hash and query into the hash map.
 *
 * @return The pointer to the data associated with the hash of `key`, otherwise NULL if the hash of `key` wasn't in the hash map, `map` or `key` was NULL or utl_hash_table_remove() failed.
 *
 * @memberof utl_hash_map
 */
void *utl_hash_map_remove(struct utl_hash_map *map, const void *key);

/** Iterate over the data in a hash map.
 *
 * @note The hash map cannot guarantee order.
 *
 * @param map The hash map to interate through.
 * @param user_data Data to be provided while iterating through the hash map.
 * @param iterate The callback to call on each entry in `map`.
 *
 * @return True if iteration was successful, otherwise false if `map` or `iterate` was NULL.
 *
 * @memberof utl_hash_map
 */
bool utl_hash_map_iterate(const struct utl_hash_map *map, void *user_data, utl_iterator_func iterate);

#endif // UTL_HASH_MAP_H_
