#ifndef UTL_VECTOR_H_
#define UTL_VECTOR_H_

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <utl/common.h>
#include <utl/unsized_vector.h>

/** @brief Iterate through a vector.
 *
 * @param pos The variable which will store the current element in the iteration.
 * @param vector The pointer to the vector that is being iterated through.
 *
 * @memberof utl_vector
 */
#define utl_vector_for_each(pos, vector) \
    for ((pos) = (vector)->vec.data; (const uint8_t*)(pos) != (const uint8_t*)(vector)->vec.data + ((vector)->element_size * utl_vector_length((vector))); (pos)++)

/** @brief Iterate through a vector in reverse.
 *
 * @param pos The variable which will store the current element in the iteration.
 * @param vector The pointer to the vector that is being iterated through.
 *
 * @memberof utl_vector
 */
#define utl_vector_for_each_rev(pos, vector) \
    for ((pos) = (vector)->vec.data + ((vector)->element_size * (utl_vector_length((vector)) - 1)); (const uint8_t*)(pos) >= (const uint8_t*)(vector)->vec.data; (pos)--)

/** @brief Get the index of an element pointer from a utl_vector.
 *
 * @param vector The vector to get the index from.
 * @param pos The data to search for in `vector`.
 * 
 * It is recommended that this macro is only used during iteration and the pointer should
 * only be from the vector caught during iteration. Using this macro after getting the data
 * and also inserting data (or really any other operation like a swap) is undefined,
 * doing so could result in bad pointer arithmatic.
 *
 * @memberof utl_vector
 */
#define utl_vector_element_index(vector, pos) \
    ((((const void*)pos - (const void*)(vector)->vec.data)) / (vector)->element_size)

/** @brief Create a utl_vector from an array and length.
 *
 * This macro creates what is in essence a slice using a utl_vector.
 * Allowing the user to make use of the utl_vector api over data that doesn't
 * necessarily need to be allocated by the utl_vector. Furthermore, slices
 * with arbitrary precision can be created.
 *
 * @param arr The array to use in the vector.
 * @param len The number of elements in `arr`.
 *
 * @memberof utl_vector
 */
#define utl_vector_slice(arr, len) \
    (const struct utl_vector) { \
        .vec = utl_unsized_vector_slice((arr), ((len) * sizeof((arr)[0]))), \
        .element_size = sizeof((arr)[0]), \
    }

/** @brief Create a utl_vector from an array literal.
 *
 * @param arr The array literal to create a vector from.
 *
 * @memberof utl_vector
 */
#define utl_vector_literal(arr) \
    utl_vector_slice((arr), sizeof((arr)) / sizeof((arr)[0]))

/** @struct utl_vector
 *
 * @brief A dynamic sized array.
 *
 * This is a basic heap allocated, dynamic, **sized** array. Each element
 * in the array must be the same size. It provides basic functions for
 * managing the underlying data.
 *
 * @warning All pointers that are retrieved from the vector become invalid
 * after a reallocation, so it is generally not recommended that pointers are held
 * unless the user knows that insertions will not happen after a certain point.
 *
 * @sa utl_unsized_vector
 */
struct utl_vector {
    struct utl_unsized_vector vec;
    size_t element_size; // !< size of each element in bytes
};

/** Initialize a vector.
 *
 * @param vector Vector to initialize.
 * @param element_size The non-zero size in bytes of each element.
 *
 * @return True if initialization was successful, otherwise false if `vector` was NULL or `element_size` was zero.
 *
 * @memberof utl_vector
 */
bool utl_vector_init(struct utl_vector *vector, size_t element_size);

/** Deinitialize a vector.
 *
 * @note This function only deinitializes the vector itself,
 * it doesn't not touch the underlying elements.
 * If the desire is to handle the destruction of the elements the user should
 * use utl_vector_elements_deinit().
 *
 * @param vector Vector to deinitialize.
 *
 * @return True if deinitialization was successful, otherwise false if `vector` was NULL.
 *
 * @memberof utl_vector
 */
bool utl_vector_deinit(struct utl_vector *vector);

/** Deinitialize a vector and handle it's elements.
 *
 * @param vector Vector to deinitialize.
 * @param destroy Function to call on each element.
 *
 * @return True if deinitialization was successful, otherwise false if `vector` or `destroy` is NULL.
 *
 * @memberof utl_vector
 */
bool utl_vector_elements_deinit(struct utl_vector *vector, utl_destroy_func destroy);

/** Get the length of a vector (number of elements).
 *
 * @warning For the sake of simplicity this function does not check for NULL in `vector`.
 *
 * @param vector The vector to get the length of.
 *
 * @return The length of the vector.
 *
 * @memberof utl_vector
 */
uint32_t utl_vector_length(const struct utl_vector *vector);

/** Copy a vector's data into another.
 *
 * @param vector The vector to copy into.
 * @param source The vector to copy from.
 *
 * @return True if successful, otherwise false if `vector` or `source` was NULL or resizing failed.
 *
 * @memberof utl_vector
 */
bool utl_vector_copy(struct utl_vector *vector, const struct utl_vector *source);

/** Grow a vector by an amount of elements at an index and provide a pointer to the new element(s).
 *
 * @note If `index` is `vector.length` then new space is appended to the vector
 * and no move is performed.
 *
 * @param vector The vector to grow.
 * @param index The index to grow at, must be equal to or smaller than `vector.length`.
 * @param amnt The non-zero amount of elements to grow `vector` by.
 *
 * @return A pointer to the start of the new elements, otherwise NULL if `vector` is NULL, `index` is too large, `amnt` is zero or resizing fails.
 *
 * @memberof utl_vector
 */
void *utl_vector_grow(struct utl_vector *vector, uint32_t index, uint32_t amnt);

/** Grow a vector at it's end and provide a pointer to the new element.
 *
 * @param vector The vector to add to.
 *
 * @return A pointer to the new element, otherwise NULL if `vector` is NULL or resizing fails.
 *
 * @memberof utl_vector
 */
void *utl_vector_add(struct utl_vector *vector);

/** Insert data into a new element in a vector at an index.
 *
 * @note This function memcpys `data` into the newly added element.
 * Providing `data` that is not the same size as `utl_vector.element_size` will cause unknown behavior.
 *
 * @param vector The vector to insert into.
 * @param index The index at which to insert `data`. The valid index range is `[0, vector.length)`.
 * @param data The new data to copy into `vector`.
 *
 * @return A pointer to the new element, otherwise NULL if `vector` was NULL, `index` was too large or resizing failed.
 *
 * @memberof utl_vector
 */
void *utl_vector_insert(struct utl_vector *vector, uint32_t index, const void *data);

/** Insert data into a new element at the end of a vector.
 *
 * @note This function memcpys `data` into the newly added element.
 * Providing `data` that is not the same size as `utl_vector.element_size` will cause unknown behavior.
 *
 * @param vector The vector to insert into.
 * @param data The new data to copy into `vector`.
 *
 * @return A pointer to the new element, otherwise NULL if `vector` was NULL, `data` was NULL or resizing failed.
 *
 * @memberof utl_vector
 */
void *utl_vector_push(struct utl_vector *vector, const void *data);

/** Get an element from a vector.
 *
 * @param vector The vector to get an element from.
 * @param index The index of an element in `vector`. The valid index range is `[0, vector.length)`.
 *
 * @return A pointer to the element at `index`, otherwise NULL if `index` too large.
 *
 * @memberof utl_vector
 */
void *utl_vector_get(const struct utl_vector *vector, uint32_t index);

/** Remove multiple elements from a vector.
 *
 * @param vector The vector to shrink.
 * @param index The index to shrink `vector` at. The valid index range is `[0, vector.length - amnt]`.
 * @param amnt The non-zero amount of element to remove from `vector` at `index`. Must also be smaller than `vector.length`.
 *
 * @return True if `vector` was successfully shrunk, or false if `vector` was NULL, `index` was too large or `amnt` was zero or too large.
 *
 * @memberof utl_vector
 */
bool utl_vector_shrink(struct utl_vector *vector, uint32_t index, uint32_t amnt);

/** Remove an element from a vector.
 *
 * @param vector The vector to get data from.
 * @param index The index in `vector`. The valid index range is `[0, vector.length)`.
 *
 * @return True if removal was successful, or false if `vector` was NULL or `index` was too large.
 *
 * @memberof utl_vector
 */
bool utl_vector_remove(struct utl_vector *vector, uint32_t index);

/** Swap two elements in a vector.
 *
 * @param vector The vector to get data from.
 * @param index1 The first index in `vector` to swap with. The valid index range is `[0, vector.length)`.
 * @param index2 The second index in `vector` to swap with. The valid index range is `[0, vector.length)`.
 *
 * @return True if swap was successful, or false if `index1` and `index2` were the same or either were too large.
 *
 * @memberof utl_vector
 */
bool utl_vector_swap(struct utl_vector *vector, uint32_t index1, uint32_t index2);

/** Perform a linear search for data in a vector.
 *
 * @note `data` is not checked for NULL. If you don't want NULL in `data` check it in `cmp`.
 *
 * @param vector The vector to get `data` from.
 * @param cmp The function to compare elements in `vector`.
 * @param data The data to compare with elements in `vector`.
 *
 * @return The index of the data or -1 if not found or `vector` or `cmp` was NULL.
 *
 * @memberof utl_vector
 */
int32_t utl_vector_cmp_find(const struct utl_vector *vector, utl_cmp_func cmp, const void *data);

#endif // UTL_VECTOR_H_
