#ifndef UTL_UNSIZED_VECTOR_H_
#define UTL_UNSIZED_VECTOR_H_

#include <utl/common.h>
#include <stdbool.h>

/** @brief Create a utl_unsized_vector from an array and length.
 *
 * This macro creates what is in essence a slice using a utl_unsized_vector.
 * Allowing the user to make use of the utl_unsized_vector api over data that doesn't
 * necessarily need to be allocated by the utl_unsized_vector. Furthermore, slices
 * with arbitrary precision can be created.
 *
 * @param arr The array to use in the vector.
 * @param len The size of `arr`.
 *
 * @memberof utl_unsized_vector
 */
#define utl_unsized_vector_slice(arr, len) \
    (const struct utl_unsized_vector){ \
        .data = (arr), \
        .allocated = 0, \
        .size = (len), \
    }

/** @brief Create an utl_unsized_vector from an array literal.
 *
 * @param arr The array literal to create an unsized vector from.
 *
 * @memberof utl_unsized_vector
 */
#define utl_unsized_vector_literal(arr) \
    utl_unsized_vector_slice((arr), sizeof((arr)) / sizeof((arr)[0]))

/** @struct utl_unsized_vector
 *
 * @brief A dynamic unsized array.
 *
 * This is a basic heap allocated, dynamic, **unsized** array.
 * It provides functionality to grow and shrink the array.
 *
 * @note If functionality for sized elements are desired use utl_vector instead.
 *
 * @warning All pointers that are retrieved from the vector become invalid
 * after a reallocation, so it is generally not recommended that pointers are held
 * unless the user knows that reallocations will not happen after a certain point.
 *
 * @see utl_vector
 */
struct utl_unsized_vector {
    void *data;
    size_t allocated, //!< bytes allocated in vector
           size; //!< bytes out of the allocated size is being used
};

/** Initialize an unsized vector.
 *
 * @param vector Unsized vector to initialize.
 *
 * @return True if successful, otherwise false if `vector` was NULL.
 *
 * @memberof utl_unsized_vector
 */
bool utl_unsized_vector_init(struct utl_unsized_vector *vector);

/** Deinitialize an unsized vector.
 *
 * @param vector Unsized vector to deinitalize.
 *
 * @return True if successful, otherwise false if `vector` was NULL.
 *
 * @memberof utl_unsized_vector
 */
bool utl_unsized_vector_deinit(struct utl_unsized_vector *vector);

/** Copy an unsized vector into another.
 *
 * @param vector Unsized vector to copy into.
 * @param source Unsized vector to copy from.
 *
 * @return True if copy was successful, otherwise false if `vector` or `source` was NULL or an allocation error occurred.
 *
 * @memberof utl_unsized_vector
 */
bool utl_unsized_vector_copy(struct utl_unsized_vector *vector, const struct utl_unsized_vector *source);

/** Grow an unsized vector at a byte index and provide a pointer to the memory.
 *
 * @param vector Unsized vector to grow
 * @param at The byte index where to grow the array. The valid byte index range is `[0, vector.size)`.
 * @param bytes The non-zero bytes to grow the array by.
 *
 * @return Pointer to memory in the vector at `at` of size `bytes`, otherwise NULL if `at` is too large, `vector` is NULL, `bytes` is zero, or resizing fails.
 *
 * @memberof utl_unsized_vector
 */
void *utl_unsized_vector_grow(struct utl_unsized_vector *vector, size_t at, size_t bytes);

/** Grow an unsized vector and provide a pointer to the memory.
 *
 * @param vector Unsized vector to grow
 * @param bytes The non-zero bytes to grow the array by.
 *
 * @return Pointer to the memory in the vector of size `bytes`, otherwise NULL if `vector` is NULL, `bytes` is zero or resizing fails.
 *
 * @memberof utl_unsized_vector
 */
void *utl_unsized_vector_add(struct utl_unsized_vector *vector, size_t bytes);

/** Shrink an unsized vector at a byte index.
 *
 * @param vector Unsized vector to shrink.
 * @param at The byte index where the vector will shrink.  The valid byte index range is `[0, vector.size - bytes)`.
 * @param bytes The non-zero bytes that the vector will shrink by. Must also be smaller than `vector.size`.
 *
 * @return True if successfull, otherwise false if `at` is too large, `vector` is NULL, or `bytes` is zero or too large.
 *
 * @memberof utl_unsized_vector
 */
bool utl_unsized_vector_shrink(struct utl_unsized_vector *vector, size_t at, size_t bytes);

/** Shrink an unsized vector.
 *
 * @param vector Unsized vector to shrink.
 * @param bytes The non-zero bytes that the vector will shrink by.
 *
 * @return True if successful, otherwise false if `vector` is NULL or `bytes` is zero.
 *
 * @memberof utl_unsized_vector
 */
bool utl_unsized_vector_remove(struct utl_unsized_vector *vector, size_t bytes);

#endif  // UTL_UNSIZED_VECTOR_H_
