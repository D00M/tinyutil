#ifndef UTL_EVENT_LOOP_SIGNAL_H_
#define UTL_EVENT_LOOP_SIGNAL_H_

#include <utl/event_loop/loop.h>
#include <stdint.h>

#ifdef UTL_EVENT_LOOP_KQUEUE

#include <sys/event.h>

#endif // UTL_EVENT_LOOP_KQUEUE

typedef void (*utl_event_signal_func)(struct utl_event *event, int signal, void *data);

/** @struct utl_event_signal
 *
 * @brief A posix signal event.
 *
 * @warning This event blocks it's respective posix signal.
 * If you plan on using `fork(2)` or using any functions in the `exec(3)` family,
 * the signal will remained blocked and cause errors for child processes. So before spawning a new process,
 * the user may want to temporarily unblock the signal using `sigprocmask(3)`.
 *
 * @sa utl_event_loop
 * @sa utl_event
 */
struct utl_event_signal {
    struct utl_event event;
    utl_event_signal_func func;
    int signal; //!< The posix signal for the event.

#ifdef UTL_EVENT_LOOP_KQUEUE

    struct kevent kevent;

#endif // UTL_EVENT_LOOP_KQUEUE
};

/** Add a posix signal to an event loop.
 *
 * @param loop The event loop to add to.
 * @param event_func The callback to be ran when `signal` is raised.
 * @param signal The signal to handle.
 * @param data User data to be provided in `event_func` when the event is dispatched. Can be NULL.
 *
 * @return A signal event if successful, otherwise NULL if either `loop` or `event_func` is NULL or utl_event_loop_add_event() failed.
 *
 * @memberof utl_event_loop
 */
struct utl_event *utl_event_loop_add_signal(struct utl_event_loop *loop, utl_event_signal_func event_func, int signal, void *data);

/** Update the signal event to handle a different signal.
 *
 * @param event The signal event to update. Must be utl_event_signal based.
 * @param signal The posix signal to handle.
 *
 * @return True if update was successful, otherwise false if `event` was NULL or an underlying function failed.
 *
 * @memberof utl_event
 */
bool utl_event_signal_update(struct utl_event *event, int signal);

/** Try to get a utl_event_signal from a utl_event
 *
 * @param event The event to try and get a utl_event_timer from.
 *
 * @return The backing utl_event_signal, otherwise NULL if `event` was either NULL or not utl_event_signal based.
 *
 * @memberof utl_event
 */
struct utl_event_signal *utl_event_signal_try_from_event(struct utl_event *event);

#endif // UTL_EVENT_LOOP_SIGNAL_H_
