#ifndef UTL_EVENT_LOOP_TIMER_H_
#define UTL_EVENT_LOOP_TIMER_H_

#include <utl/event_loop/loop.h>
#include <stdbool.h>
#ifdef UTL_EVENT_LOOP_KQUEUE
#include <sys/event.h>
#endif // UTL_EVENT_LOOP_KQUEUE

typedef void (*utl_event_timer_func)(struct utl_event *event, void *data);

/** @struct utl_event_timer
 *
 * @brief A timer event.
 *
 * @sa utl_event_loop
 * @sa utl_event
 */
struct utl_event_timer {
    struct utl_event event;
    utl_event_timer_func func;

#ifdef UTL_EVENT_LOOP_KQUEUE

    struct kevent kevent;

#endif // UTL_EVENT_LOOP_KQUEUE
};

/** Add a timer to an event loop.
 *
 * @note The newly created timer returned is disarmed, in order to arm it
 * the user must call `utl_event_timer_update()` to set the timer.
 *
 * @param loop The event loop to add a timer to.
 * @param event_func The function to run once the timer goes off.
 * @param data User data to provide in `event_func` when the timer goes off. Can be NULL.
 *
 * @return A timer based event if successful, otherwise NULL if `loop` or `event_func` was NULL or utl_event_loop_add_event() failed.
 *
 * @memberof utl_event_loop
 */
struct utl_event *utl_event_loop_add_timer(struct utl_event_loop *loop, utl_event_timer_func event_func, void *data);

/** Arm or Disarm a timer backed event.
 *
 * @param event The timer based event to arm or disarm.
 * @param ms_delay The time, in milliseconds, to be used as the deadline for the timer. If 0 the timer is disarmed.
 *
 * @return True if the update was successful, otherwise false if `event` was either NULL or not a utl_event_timer based event or an underlying function failed.
 *
 * @memberof utl_event
 */
bool utl_event_timer_update(struct utl_event *event, uint32_t msecs);

/** Try to get a utl_event_timer from a utl_event
 *
 * @param event The event to try and get a utl_event_timer from.
 *
 * @return The backing utl_event_timer, otherwise NULL if `event` was either NULL or not utl_event_timer based.
 *
 * @memberof utl_event
 */
struct utl_event_timer *utl_event_timer_try_from_event(struct utl_event *event);

#endif // UTL_EVENT_LOOP_TIMER_H_
