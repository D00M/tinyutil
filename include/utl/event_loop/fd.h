#ifndef UTL_EVENT_LOOP_FD_H_
#define UTL_EVENT_LOOP_FD_H_

#include <utl/event_loop/loop.h>

typedef void (*utl_event_fd_func)(struct utl_event *event, uint32_t fd, uint32_t mask, void *data);

/** @struct utl_event_fd
 *
 * @brief A file descriptor event.
 *
 * @sa utl_event_loop
 * @sa utl_event
 */
struct utl_event_fd {
    struct utl_event event;
    utl_event_fd_func func;
    int fd; //!< The original file descriptor provided, should only be closed by the caller of utl_event_loop_add_fd().
};

/** Add a file descriptor to an event loop.
 *
 * @param loop The event loop to add to.
 * @param event_func The function to run upon dispatching this event.
 * @param fd The file descriptor to add.
 * @param mask The utl_event_mask to register `fd` for triggering.
 * @param data User data provided in `event_func` once the event is dispatched. Can be NULL.
 *
 * @return A file descriptor event if successful, otherwise NULL if `loop` or `event_func` was NULL, `mask` contained neither UTL_EVENT_READABLE or UTL_EVENT_WRITABLE or utl_event_loop_add_event() failed.
 *
 * @memberof utl_event_loop
 */
struct utl_event *utl_event_loop_add_fd(struct utl_event_loop *loop, utl_event_fd_func event_func, int fd, uint32_t mask, void *data);

/** Update a file descriptor event with a new mask.
 *
 * @param event The event to update the mask of. **Must** be a utl_event_fd based event.
 * @param mask The new utl_event_mask to trigger the event.
 *
 * @return True if the update was sucessful, otherwise false if `mask` contained neither UTL_EVENT_READABLE or UTL_EVENT_WRITABLE, `event` either NULL or not a utl_event_fd based event or utl_event_loop_update_event() failed.
 *
 * @memberof utl_event
 */
bool utl_event_fd_update(struct utl_event *event, uint32_t mask);

/** Determine if a utl_event is utl_event_fd based.
 *
 * @param event The event to check for being utl_event_fd based.
 *
 * @return True if `event` is utl_event_fd based, false if `event` was either NULL or not utl_event_fd based.
 *
 * @memberof utl_event
 */
struct utl_event_fd *utl_event_fd_try_from_event(struct utl_event *event);

#endif /* ifndef UTL_EVENT_LOOP_FD_H_ */
