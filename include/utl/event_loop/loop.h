#ifndef UTL_EVENT_LOOP_H_
#define UTL_EVENT_LOOP_H_

#include <utl/list.h>
#include <stdint.h>

/** @page event_loop Event Loop
 *
 * The utl_event_loop is portable, easy to use and extend.
 * The express goal of this event loop was to provide a simple interface to allow
 * users to create both portable and effcient targeted programs.
 *
 * Currently the event loop only supports Linux's epoll and BSD's kqueue. 
 * Built-in the event loop provides a file descriptor, timer and signal event.
 * The timer and signal events support signalfd and timerfd as well as their kevent counterparts.
 *
 * To start using the utl_event_loop. Use `utl_event_loop_init()`, then
 * begin adding events.
 * ```c
 * struct utl_event_loop loop;
 * utl_event_loop_init(&loop);
 *
 * struct utl_event *fd = utl_event_loop_add_fd(&loop, fd_callback, fd, UTL_EVENT_READABLE, NULL);
 *
 * struct utl_event *timer = utl_event_loop_add_timer(&loop, timer_callback, NULL);
 * utl_event_timer_update(timer, 10000); // Time is in milliseconds
 *
 * struct utl_event *signal = utl_event_loop_add_signal(&loop, signal_callback, SIGUSR1, NULL); // Blocks the provided signal
 * ```
 *
 * Then begin waiting for events:
 * ```c
 * utl_event_loop_wait(&loop, -1); // Pass -1 to wait indefinitely otherwise pass a timeout in milliseconds
 * ```
 *
 * Once finished with events and/or the event loop:
 * ```c
 * utl_event_remove(timer); // Remove an event before event loop destruction if it isn't needed
 * utl_event_loop_deinit(&loop); // Removes and destroys events as well.
 * ```
 *
 * For more information about creating custom event types see @ref custom_event_types.
 */

/** @page custom_event_types Creating Custom Event Types
 * 
 * The @ref utl_event structure is an opaque type that is used to interact with
 * the event loop. It contains basic information used by the event loop
 * to register and dispatch the event. Any event type will use a utl_event along with
 * the functions, `utl_event_loop_add_event()`, `utl_event_loop_update_event()` 
 * and `utl_event_loop_remove_event()` to register, update and remove the event
 * from the event loop.
 *
 * Simply speaking, a utl_event structure is the only required part to making an
 * event. The utl_event only needs to be initialized, requiring an interface, the 
 * event loop it's attached to, and an event type and data relating to it's specific
 * event. The `type` and `data` members of a utl_event compose a tagged union, 
 * the data tied to it's type will be used by the event loop to register the event.
 *
 * On a system which supports file descriptors, `UTL_EVENT_TYPE_FD` will be available as
 * a value for `utl_event_type` along with an accompanying entry in `utl_event_data`. 
 * This can be used to present a file descriptor along with bitmask of the values from `utl_event_mask`,
 * of which only values `UTL_EVENT_READABLE` and `UTL_EVENT_WRITABLE` will be used, the rest ignored 
 * (`UTL_EVENT_HANGUP` and `UTL_EVENT_ERROR` are only provided on dispatch to the event).
 * On BSD systems, a `UTL_EVENT_TYPE_KEVENT` will be available along with an entry in `utl_event_data` 
 * which allows the event to submit an array of kevents. With a few pre-processor conditions an event 
 * can easily target BSD systems and still be compatible with Linux systems.
 * If an event were going to be a file system watcher, given some conditions, it could support not only 
 * `inotify(7)` from Linux but also use EVFILT_VNODE from kqueue within a single generic file system monitoring
 * event type.
 *
 * @note The kqueue event loop implementation supports both file descriptors and kevents, the backend
 * will expand file descriptors into kevents. So if an event will only use file descriptors there is no
 * need to conditionally compile in kevents.
 *
 * Typically, an event type will comprise of at least a structure, let's say `foo_event`
 * and a utl_event field, in this case `event`:
 * ```c
 * struct foo_event {
 *  struct utl_event event;
 *  // More data specific to this event.
 * };
 * ```
 *
 * Given that `foo_event` adds a file descriptor to the event loop:
 * ```c
 * struct utl_event *event foo_event_create(struct utl_event_loop *loop, int fd) {
 *  struct foo_event *foo = calloc(1, sizeof(*event));
 *
 *  foo->event.loop = loop;
 *  foo->event.interface = &foo_event_interface; // Event interface deals with destruction and dispatching the event when triggered.
 *  foo->event.type = UTL_EVENT_TYPE_FD;
 *  foo->event.data.fd.fd = fd;
 *  foo->event.data.fd.mask = UTL_EVENT_READABLE | UTL_EVENT_WRITABLE; // Any values other than these are ignored.
 *  foo->event.user_data = NULL; // Add user data if you want to.
 *
 *  if (utl_event_loop_add_event(loop, &foo->event) == false) {
 *      // error
 *  }
 *
 *  return &foo->event; // So the user can remove it later.
 * }
 * ```
 *
 * Once `foo_event` is triggered it's dispatch function in it's interface is called:
 * ```c
 * void foo_event_dispatch(struct utl_event *event, const union utl_event_data data) {
 *  struct foo_event *foo = utl_container_of(event, foo, event); // In this case, it is expected to fetch foo_event this way.
 *
 *  // Since foo_event is of type UTL_EVENT_TYPE_FD, data.fd is filled with the file descriptor of the event and it's triggered event mask.
 *  // Check data.fd.mask and do some calculations.
 *  ....
 * }
 * ```
 */

struct utl_event;
struct utl_event_loop;

/** @enum utl_event_mask
 *
 * @brief The bitmask values for a utl_event.
 *
 * @sa utl_event
 */
enum utl_event_mask {
    UTL_EVENT_READABLE = 1 << 0,
    UTL_EVENT_WRITEABLE = 1 << 1,
    UTL_EVENT_ERROR = 1 << 2,
    UTL_EVENT_HANGUP = 1 << 3,
};

/** @enum utl_event_type
 *
 * @brief The data type of the utl_event.
 *
 * This enum specifies an event's type, it is used along-side the utl_event_data union
 * to act as a tagged union. This enum specifies how the utl_event_loop should manage
 * the event's data when registering and dispatching the event.
 *
 * @sa utl_event_data
 */
enum utl_event_type {

#ifdef UTL_EVENT_LOOP_FD

    UTL_EVENT_TYPE_FD,

#endif // UTL_EVENT_LOOP_FD

#ifdef UTL_EVENT_LOOP_KQUEUE

    UTL_EVENT_TYPE_KEVENT,

#endif // UTL_EVENT_LOOP_KQUEUE

};

/** @union utl_event_data
 *
 * @brief utl_event type specific data.
 *
 * This data is used to not only specify the data to use to register and manage
 * the utl_event it is attached to. It is also given to the event's dispatch function
 * to convey more information about the triggering of the event.
 *
 * @sa utl_event_type
 */
union utl_event_data {

#ifdef UTL_EVENT_LOOP_FD

    // For UTL_EVENT_TYPE_FD
    struct {
        int fd;
        uint32_t mask; // enum utl_event_mask
    } fd;

#endif // UTL_EVENT_LOOP_FD

#ifdef UTL_EVENT_LOOP_KQUEUE

    // For UTL_EVENT_TYPE_KEVENT
    struct {
        struct kevent *arr;
        uint32_t size;
    } kevents;

#endif // UTL_EVENT_LOOP_KQUEUE

};

/** @struct utl_event_interface
 * 
 * @brief The interface for a utl_event.
 *
 * @sa utl_event
 */
struct utl_event_interface {
    bool (*destroy)(struct utl_event *event);

    /// If `event` is UTL_EVENT_TYPE_FD then `data` contains the fd and the triggered event mask.
    /// If `event` is UTL_EVENT_TYPE_KEVENT then `data` contains the triggered kevents tied to this event from the kevent() syscall.
    bool (*dispatch)(struct utl_event *event, const union utl_event_data data);
};

/** @struct utl_event
 *
 * @brief Opaque event type to be registered to a utl_event_loop.
 *
 * For more information on how to create an event type see @ref custom_event_types.
 *
 * @sa utl_event_loop
 */
struct utl_event {
    struct utl_event_loop *loop;
    const struct utl_event_interface *interface;
    enum utl_event_type type;
    union utl_event_data data; //!< The event data that defines it's use in the event loop.
    void *user_data;

    struct utl_list link;
};

/** @struct utl_event_loop
 *
 * @brief A portable event loop.
 *
 * For quick start guide see @ref event_loop.
 *
 * @sa utl_event
 */
struct utl_event_loop {
    int fd; //!< We currently only support epoll and kqueue so this is really all we need.
    struct utl_list events;
};

/** Initialize an event loop.
 *
 * @param loop The event loop to initialze.
 *
 * @return True if initialization was successful, otherwise false if `loop` was NULL or an internal call failed.
 *
 * @memberof utl_event_loop
 */
bool utl_event_loop_init(struct utl_event_loop *loop);

/** Deinitialize an event loop.
 *
 * @param loop The event loop to deinitialize.
 *
 * @return True if initialization was successful, otherwise false if `loop` was NULL.
 *
 * @memberof utl_event_loop
 */
bool utl_event_loop_deinit(struct utl_event_loop *loop);

/** Add an event to and event loop.
 *
 * @param loop The event loop to add and event to.
 * @param event The event to add.
 *
 * @return True if the addition was sucessful, otherwise false if `loop` or `event` was NULL or an internal call failed.
 *
 * @memberof utl_event_loop
 */
bool utl_event_loop_add_event(struct utl_event_loop *loop, struct utl_event *event);

/** Update an event in an event loop.
 *
 * It is assumed that the state of `event` is the new state and the dispatch
 * conditions of the event in the backend will be updated to reflect the new
 * state.
 *
 * @param loop The event loop to update an event in.
 * @param event The event to update in the event loop.
 *
 * @return True if update was sucessful, otherwise false if `loop` or `event` was NULL or an internal call failed.
 *
 * @memberof utl_event_loop
 */
bool utl_event_loop_update_event(struct utl_event_loop *loop, struct utl_event *event);

/** Remove an event from the event loop.
 *
 * @param loop The event loop to remove an event from.
 * @param event The event to remove.
 * 
 * @return True if removal was sucessful, otherwise false if `loop` or `event` was NULL or an internal call failed.
 *
 * @memberof utl_event_loop
 */
bool utl_event_loop_remove_event(struct utl_event_loop *loop, struct utl_event *event);

/** Wait for an event to trigger.
 *
 * @param loop The event loop to wait on.
 * @param timeout The amount of time to attempt to wait for an event to trigger in milliseconds. If -1 then wait indefinitely.
 *
 * @return The amount of events dispatched in the event loop, otherwise -1 if `loop` was NULL or an internal call failed.
 *
 * @memberof utl_event_loop
 */
int32_t utl_event_loop_wait(struct utl_event_loop *loop, int32_t timeout);

/** Remove an event from an event loop.
 *
 * @param event The event to remove.
 *
 * @memberof utl_event
 */
bool utl_event_remove(struct utl_event *event);

#endif // !UTL_EVENT_LOOP_H_
