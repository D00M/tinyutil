#ifndef UTL_HASH_TABLE_H_
#define UTL_HASH_TABLE_H_

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <utl/common.h>

/** @struct utl_hash_table_entry
 *
 * @brief An entry a hash table.
 *
 * @memberof utl_hash_table
 */
struct utl_hash_table_entry {
    uint32_t hash;
    void *data;
};

/** @struct utl_hash_table
 *
 * @brief A hash table.
 *
 * This base hashing type can be extended to or treated as a
 * hash map or hash set. It provides the basic functionality of
 * adding hashed data, and looking them up in the table.
 *
 * @note Basically just ripped this from libweston.
 * I could've made this myself (copium) but I liked their implemenation better.
 */
struct utl_hash_table {
    struct utl_hash_table_entry *entries; //!< array of entries
    uint32_t hash_sizes_index,            //!< an internal value
             amnt,                        //!< the amount of entries
             allocated,                   //!< the allocated entries
             deleted;                     //!< the deleted entries
};

/** Initialize a hash table.
 *
 * @param table The hash table to initalize.
 *
 * @return true if initalization was successful, otherise false if `table` was NULL or an allocation failed.
 *
 * @memberof utl_hash_table
 */
bool utl_hash_table_init(struct utl_hash_table *table);

/** Deinitialize a hash table
 *
 * @note This function only deinitializes the hash table,
 * if the user wishes to act on the entries before deinitialization
 * they should use utl_hash_table_elements_deinit().
 *
 * @param table The hash table to destroy.
 *
 * @return True if deinitialization was successful, otherwise false if `table` was NULL.
 *
 * @memberof utl_hash_table
 */
bool utl_hash_table_deinit(struct utl_hash_table *table);

/** Deinitialize a hash table and handle its entries
 *
 * @param table The hash table to deinitialize.
 * @param destroy The function to call upon each entry.
 *
 * @return True if deinitialization was successful, otherwise false if `table` or `destroy` was NULL.
 *
 * @memberof utl_hash_table
 */
bool utl_hash_table_elements_deinit(struct utl_hash_table *table, utl_destroy_func destroy);

/** Insert a hash and the associated data into a hash table.
 *
 * @param table The hash table to insert into.
 * @param hash The hash to be associated with `data`.
 * @param data The data to be associated with `hash`.
 *
 * @return True if an entry was succcessfully added, otherwise false if `table` or `data` was NULL or the hash already existed in `table`.
 *
 * @memberof utl_hash_table
 */
bool utl_hash_table_insert(struct utl_hash_table *table, uint32_t hash, void *data);

/** Remove an entry from a hash table.
 *
 * @param table The table to remove from.
 * @param hash The hash to lookup and remove an entry.
 *
 * @return The pointer to the data associated with the hash, otherwise NULL if the hash wasn't found in the table or `table` was NULL.
 *
 * @memberof utl_hash_table
 */
void *utl_hash_table_remove(struct utl_hash_table *table, uint32_t hash);

/** Get the data associated with hash in a hash table.
 *
 * @param table The table to get from.
 * @param hash The hash to lookup in the hash table.
 *
 * @return The pointer to the data associated with the hash, otherwise NULL if the hash wasn't found in `table` or `table` was NULL.
 *
 * @memberof utl_hash_table
 */
void *utl_hash_table_get(const struct utl_hash_table *table, uint32_t hash);

/** Check if a hash table entry is deleted
 *
 * @param entry The entry in the hash table to check.
 *
 * @return True if `entry` has been deleted, otherwise false if it isn't deleted or `entry` was NULL.
 *
 * @memberof utl_hash_table_entry
 */
bool utl_hash_table_entry_is_deleted(const struct utl_hash_table_entry *entry);

/** Check if a hash table entry is filled with data.
 *
 * @param entry The entry in the hash table to check.
 * 
 * @return True if `entry` is filled with data, otherwise false if it isn't filled with data or `entry` was NULL.
 *
 * @memberof utl_hash_table_entry
 */
bool utl_hash_table_entry_is_filled(const struct utl_hash_table_entry *entry);

#endif // UTL_HASH_TABLE_H_
