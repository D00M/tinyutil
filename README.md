# tinyutil {#mainpage}
A simple C data structure library. tinyutil provides common data structures built for composability and ease of use.

# Goals
1. Composability
2. Ease of use
3. Extensability
4. Portability

# Why tinyutil?
I found myself building basic data structures in the same way often, across multiple projects, or just completely copying code.
So I though it might be useful to separate these structures into their own library for ease of use.
If you find yourself building data structures like these often, or are simply looking for simple implmentations of your favorite
data structures then this library might be for you.
If the specific data structure you'd like isn't here, the building blocks for it probably are. If you want a stack just
extend the interface for the `utl_vector` to have a push and pop function. All of the structures in this library are open
so reaching in and changing them manually is simple.

# Compiling, Distributing and Testing
This library can be compiled into a single object file or into a shared library.

For a single object file:
```
make static
```

For a shared library:
```
make dynamic
```

For a compressed archive of the headers for distribution:
```
make dist
```

To run tests on tinyutil:
```
make test
```

# TODO
+ [x] Get tests working.
+ [x] Label `utl_vector_get()` as const for `vector` arg. Should probably do this library wide because not doing so limits the implementations that can be done on top of the structures in the library.
+ [x] Change `utl_cmp_func()` typedef to return `int64_t` so that safe pointer comparison is possible.
+ [x] Create tests for `utl_list` and `utl_event_loop`.
+ [x] Create an interface for the event loop to allow a user to implement their platform specific io multiplexing stuff. The goal is to implement this interface for the major platforms that being linux (epoll), windows (iocp), freebsd (kqueue).
    + [x] We need to somehow separate make a portable interface to add the os file descriptor / handle.
    Maybe don't? Instead just have a different event type for each system which uses a different file descriptor / handle type?
    + [x] Using gcc / clang use these arguments `-dM -E -xc /dev/null` to find out what the different predefined macros are for each system. Somehow. Or just wing it and use [cpredef](https://github.com/cpredef/predef).
    + [x] See the [kqueue](https://man.freebsd.org/cgi/man.cgi?query=kqueue&apropos=0&sektion=0&manpath=FreeBSD+15.0-CURRENT&arch=default&format=html) freebsd man page for how to use kqueue (this applies to essentially all major versions of bsd, DragonFlyBSD, FreeBSD, OpenBSD and NetBSD).
+ [ ] Test kqueue event loop backend.
