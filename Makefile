# Paths
INCLUDE  = ./include
TINYUTIL = ${INCLUDE}/utl
SRC 	 = ./src
TEST	 = ./tests

# Files
TESTS = ${TEST}/tests.c ${TEST}/hash_table_test.c ${TEST}/hash_map_test.c ${TEST}/unsized_vector_test.c ${TEST}/vector_test.c ${TEST}/ordered_vector_test.c ${TEST}/event_loop_test.c ${TEST}/list_test.c ${TEST}/signal_test.c
SRCS  = ${SRC}/common.c ${SRC}/hash_table.c ${SRC}/hash_map.c ${SRC}/unsized_vector.c ${SRC}/vector.c ${SRC}/ordered_vector.c ${SRC}/list.c ${SRC}/signal.c ${SRC}/util.c ${SRC}/event_loop/loop.c
OBJS  = ${SRCS:.c=.o}

# Compile Flags
INCLUDES = -I${INCLUDE}
CFLAGS = -Wall ${INCLUDES}
CC = cc

# Attempt to detect the system so we can add the right files and support the right event loop features.
UNAME = $(shell uname -s)
ifeq ($(UNAME), Linux)
	SRCS += ${SRC}/event_loop/epoll.c
	CFLAGS += -DUTL_EVENT_LOOP_EPOLL
	SIGNALFD = true
	TIMERFD = true
	FD = true
endif
ifeq ($(findstring BSD, $(UNAME)), BSD)
	SRCS += ${SRC}/event_loop/kqueue.c ${SRC}/event_loop/signal_kevent.c ${SRC}/event_loop/timer_kevent.c
	CFLAGS += -DUTL_EVENT_LOOP_KQUEUE
	FD = true
endif

ifdef SIGNALFD
	SRCS   += ${SRC}/event_loop/signalfd.c
	CFLAGS += -DUTL_EVENT_LOOP_SIGNALFD
endif
ifdef TIMERFD
	SRCS   += ${SRC}/event_loop/timerfd.c
	CFLAGS += -DUTL_EVENT_LOOP_TIMERFD
endif
ifdef FD
	SRCS   += ${SRC}/event_loop/fd.c
	CFLAGS += -DUTL_EVENT_LOOP_FD
endif

all: static
static: options tinyutil.a
dynamic: options tinyutil.so

options:
	@echo "=============="
	@echo "build options:"
	@echo "SRCS   = ${SRCS}"
	@echo "OBJS   = ${OBJS}"
	@echo "TESTS  = ${TESTS}"
	@echo "CFLAGS = ${CFLAGS}"
	@echo "=============="

test: CFLAGS += -fsanitize=address,undefined,bounds,enum,leak
test: FORCE clean static
	$(CC) ${CFLAGS} ${TESTS} tinyutil.a -o ${TEST}/$@
	${TEST}/$@ || true

tinyutil.a: ${OBJS}
	ar -rc $@ ${OBJS}

tinyutil.so: CFLAGS += -fPIC -shared
tinyutil.so: ${OBJS}
	$(CC) -o $@ ${OBJS} ${CFLAGS}

%.o: %.c
	$(CC) ${CFLAGS} -c $< -o $@

dist:
	mkdir -p tinyutil
	cp -r ${TINYUTIL} tinyutil
	tar -cf tinyutil.tar tinyutil
	rm -rf tinyutil

clean:
	rm -rf ${OBJS} ${TEST}/test

FORCE:
