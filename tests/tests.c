#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <string.h>
#include "util.h"
#include "tests.h"

#define MSG_LENGTH 512

typedef void (*test_func)(void);

struct test_result {
    bool passed;
    char *msg; // Only valid if passed == false
};

struct test {
    const char *name;
    test_func test;
    struct test_result result;
};

static void sigsegv(int32_t sig);
static void sigabrt(int32_t sig);
static void send_test_return(bool res, char *msg);
static void get_test_return(struct test_result *result);
static void print_test_result(struct test *test);

char *ret_msg = NULL; // The tests will use this to transfer a message from an assert.
static int32_t pipes[2];
static struct sigaction sa_segv = {
    .sa_handler = sigsegv,
}, sa_abrt = {
    .sa_handler = sigabrt,
};
static struct test tests[] = {
    {
        .name = "utl_unsized_vector",
        .test = utl_unsized_vector_test,
    },
    {
        .name = "utl_vector",
        .test = utl_vector_test,
    },
    {
        .name = "utl_ordered_vector",
        .test = utl_ordered_vector_test,
    },
    {
        .name = "utl_hash_map",
        .test = utl_hash_map_test,
    },
    {
        .name = "utl_hash_table",
        .test = utl_hash_table_test,
    },
    {
        .name = "utl_list",
        .test = utl_list_test,
    },
    {
        .name = "utl_event_loop",
        .test = utl_event_loop_test,
    },
    {
        .name = "utl_signal",
        .test = utl_signal_test,
    },
};

void sigsegv(int32_t sig) {
    send_test_return(false, "segmentation fault");
    exit(EXIT_FAILURE);
}

void sigabrt(int32_t sig) {
    send_test_return(false, ret_msg ? ret_msg : "aborted (likely from a failed assert)");
    exit(EXIT_FAILURE);
}

void send_test_return(bool res, char *msg) {
    write(pipes[1], &res, sizeof(bool));

    if (!res) {
        size_t bytes = msg ? sizeof(char) * (strlen(msg) + 1) : 0;
        write(pipes[1], &bytes, sizeof(size_t));
        if (bytes == 0) return;

        write(pipes[1], msg, bytes);
    }
}

void get_test_return(struct test_result *result) {
    read(pipes[0], &result->passed, sizeof(bool));

    if (!result->passed) {
        size_t bytes;
        read(pipes[0], &bytes, sizeof(size_t));
        if (bytes == 0) {
            result->msg = NULL;
            return;
        }

        result->msg = malloc(bytes);
        read(pipes[0], result->msg, bytes);
    }
}

void print_test_result(struct test *test) {
    printf("%c[%dm%s%c[0m %s",
            0x1B, test->result.passed ? 32 : 31, test->result.passed ? "PASSED" : "FAILED", 0x1B,
            test->name);
    if (!test->result.passed) {
        printf(" because | %s", test->result.msg);
    }
    printf("\n");
}

void run_test(struct test *test) {
#ifdef DEBUG
    printf("Running %s...\n", test->name);
#endif // DEBUG

    pid_t pid = fork();
    if (pid == 0) {
        test->test();
        send_test_return(true, NULL);
        exit(EXIT_SUCCESS);
    }

    waitpid(pid, NULL, 0);

    get_test_return(&test->result);
}

int main(int argc, char **argv) {
    uint32_t failed = 0;
    struct test *test;

    sigaction(SIGSEGV, &sa_segv, NULL);
    sigaction(SIGABRT, &sa_abrt, NULL);
    pipe(pipes);

    for (int i = 0; i < ARRAY_LENGTH(tests); i++) {
        run_test(tests + i);
    }

    for (int i = 0; i < ARRAY_LENGTH(tests); i++) {
        test = tests+i;
        print_test_result(test);
        free(test->result.msg);
        if (!test->result.passed) failed++;
    }

    return failed;
}
