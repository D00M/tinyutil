#include <stdint.h>
#include <utl/list.h>
#include "tests.h"

static uint32_t data1 = 15,
                data2 = 30;

struct thing {
    uint32_t data;
    struct utl_list link;
};

bool list_iterator(struct utl_list *link, void *data) {
    struct thing *thing = utl_container_of(link, thing, link);
    // LAZY
    assert(thing->data == data1 || thing->data == data2);
    return false;
}

void utl_list_test(void) {
    struct utl_list head, other;
    struct thing thing1, thing2;
    thing1.data = data1;
    thing2.data = data2;

    assert(utl_list_init(&head));
    assert(utl_list_init(&other));

    assert(utl_list_insert(&head, &thing1.link));

    assert(utl_list_insert(&thing1.link, &thing2.link));

    assert(utl_list_length(&head) == 2);

    struct thing *thing_ptr, *thing_tmp;
    utl_list_for_each(thing_ptr, &head, link) {
        // Not exacly an accurate test.
        assert(thing_ptr->data == data1 || thing_ptr->data == data2);
    }

    assert(utl_list_find(&head, &thing1.link) == 0);
    assert(utl_list_find(&head, &thing2.link) == 1);

    assert(utl_list_at(&head, 0) == &thing1.link);
    assert(utl_list_at(&head, 1) == &thing2.link);

    assert(utl_list_iterate_mutable(&head, list_iterator, NULL));

    assert(utl_list_insert_list(&other, &head))

    utl_list_for_each_safe(thing_ptr, thing_tmp, &other, link) {
        assert(thing_ptr->data == data1 || thing_ptr->data == data2);
        assert(!utl_list_empty(&other));
        assert(utl_list_remove(&thing_ptr->link));
    }

    assert(utl_list_length(&other) == 0 && utl_list_empty(&other));
}
