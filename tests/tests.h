#ifndef UTL_TESTS_H_
#define UTL_TESTS_H_

#include <stdbool.h>
#include <signal.h>

#define INTERNAL_STRINGIFY(str) #str
#define STRINGIFY(str) INTERNAL_STRINGIFY(str)
#define assert(expr) { \
    if (!(expr))  { \
        ret_msg = "failed assert" " :: " #expr " :: " "line:" STRINGIFY(__LINE__); \
        raise(SIGABRT); \
    } \
}
#define assert_with(expr, msg) { \
    if (!(expr)) { \
        ret_msg = msg " :: " STRINGIFY(__LINE__); \
        raise(SIGABRT); \
    } \
}

extern char *ret_msg;

void utl_vector_test(void);
void utl_unsized_vector_test(void);
void utl_ordered_vector_test(void);
void utl_hash_map_test(void);
void utl_hash_table_test(void);
void utl_list_test(void);
void utl_event_loop_test(void);
void utl_signal_test(void);

#endif  // UTL_TESTS_H_
