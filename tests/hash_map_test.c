#include <utl/hash_map.h>
#include "tests.h"
#include "util.h"

static uint32_t test_data[] = {13, 15, 83, 90, 32, 0, 75, 3, 4, 8, 1};

bool hash_map_uint32_t_iterate(const void *data, void *user_data) {
    for (int i = 0; i < ARRAY_LENGTH(test_data); i++) {
        if (*(uint32_t*)data != test_data[i]) continue;
        return true;
    }
    assert_with(false, "We failed to find num in vector_data during destruction");
    return false;
}

void utl_hash_map_test(void) {
    struct utl_hash_map map;
    assert(utl_hash_map_init(&map, utl_murmur3_uint32_t_hash));

    // Since most of the hash map's functionality is already tested through hash table,
    // we basically just want to check and make sure the hash function passed
    // in is being used correctly then make sure the iteration function works.
    for (int i = 0; i < ARRAY_LENGTH(test_data); i++) {
        assert(utl_hash_map_insert(&map, &test_data[i], &test_data[i]));
    }

    for (int i = 0; i < ARRAY_LENGTH(test_data); i++) {
        assert(test_data[i] == *(uint32_t*)utl_hash_map_get(&map, &test_data[i]));
    }

    utl_hash_map_iterate(&map, NULL, hash_map_uint32_t_iterate);
    utl_hash_map_deinit(&map);
}
