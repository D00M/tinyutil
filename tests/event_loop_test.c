#include <time.h>
#include <utl/event_loop/loop.h>
#include <utl/event_loop/fd.h>
#include <utl/event_loop/timer.h>
#include <utl/event_loop/signal.h>
#include <sys/stat.h>
#include <stdint.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include "tests.h"
#include "utl/list.h"

struct thing {
    uint32_t data;
    struct utl_list link;
};

static uint32_t num = 15;
static bool event_happend = false;
static bool signal_happened = false;

static void timer_event_func(struct utl_event *event, void *data) {
    event_happend = true;
}

static void signal_event_func(struct utl_event *event, int signal, void *data) {
    event_happend = true;
}

static void sigusr1(int32_t sig) {
    signal_happened = true;
}

static void fd_event_func(struct utl_event *event, uint32_t fd, uint32_t mask, void *data) {
    assert(mask & UTL_EVENT_READABLE);
    event_happend = true;

    struct thing *thing = data;
    uint32_t out;

    read(fd, &out, sizeof(out));

    assert(out == thing->data);
}

static int32_t fifo_setup(char **path) {
    int32_t fifo_fd;
    size_t len;
    const char *runtime_path = getenv("XDG_RUNTIME_DIR");
    assert(runtime_path);

    for (int i = 0; i < 100; i++) {
        len = snprintf(NULL, 0, "%s/tinyutil-event-loop-test-%d", runtime_path, i) + 1;
        *path = calloc(1, len);
        assert(*path);
        snprintf(*path, len, "%s/tinyutil-event-loop-test-%d", runtime_path, i);

        if (mkfifo(*path, 0666) == -1) {
            assert(errno == EEXIST);
            free(*path);
            continue;
        }

        assert((fifo_fd = open(*path, O_CLOEXEC | O_RDWR | O_NONBLOCK)) != -1);

        return fifo_fd;
    }

    return -1;
}

void utl_event_loop_test(void) {
    struct utl_event_loop loop;
    assert(utl_event_loop_init(&loop));

    // We'll just use a fifo to test the event loop file descriptor stuff.
    int fifo_fd;
    char *fifo_path;
    assert((fifo_fd = fifo_setup(&fifo_path)) != -1);

    struct thing thing;
    struct utl_event *fd_event;
    assert((fd_event = utl_event_loop_add_fd(&loop, fd_event_func, fifo_fd, UTL_EVENT_READABLE, &thing)));

    assert(utl_list_length(&loop.events) == 1);

    thing.data = num;

    assert(write(fifo_fd, &thing.data, sizeof(thing.data)) != -1);

    assert(utl_event_loop_wait(&loop, 1000) == 1);
    assert(event_happend);

    assert(utl_event_remove(fd_event));

    // Nothing should happen after removing the event
    event_happend = false;
    assert(write(fifo_fd, &thing.data, sizeof(thing.data)) != -1);
    assert(utl_event_loop_wait(&loop, 1000) == 0);
    assert(!event_happend);

    close(fifo_fd);
    unlink(fifo_path);
    free(fifo_path);

    struct utl_event *timer_event;
    assert((timer_event = utl_event_loop_add_timer(&loop, timer_event_func, NULL)));

    assert(utl_event_timer_update(timer_event, 1000));

    assert(utl_event_loop_wait(&loop, 2000) == 1);
    assert(event_happend);

    utl_event_remove(timer_event);
    event_happend = false;

    struct utl_event *signal_event;
    assert((signal_event = utl_event_loop_add_signal(&loop, signal_event_func, SIGUSR1, NULL)));

    const struct sigaction sigact = {
        .sa_handler = sigusr1,
    };

    assert(sigaction(SIGUSR1, &sigact, NULL) != -1);

    raise(SIGUSR1);

    assert(utl_event_loop_wait(&loop, 1000) == 1);

    // Make sure the signal is blocked
    assert(event_happend && !signal_happened);

    utl_event_remove(signal_event);
    event_happend = false;

    // Make sure the signal is unblocked
    raise(SIGUSR1);
    assert(signal_happened);

    assert(utl_event_loop_deinit(&loop));
}
