#include "tests.h"
#include "util.h"
#include <string.h>
#include <utl/unsized_vector.h>

static char test_str[] = "hi there neighbor!";
static char test_str2[] = "hi neighbor!";

void utl_unsized_vector_test(void) {
    struct utl_unsized_vector vector;
    assert(!utl_unsized_vector_init(NULL));
    assert(utl_unsized_vector_init(&vector));

    assert(!utl_unsized_vector_grow(&vector, 1, 10));
    assert(!utl_unsized_vector_grow(&vector, 0, 0));

    char *str;
    assert((str = utl_unsized_vector_grow(&vector, 0, ARRAY_LENGTH(test_str) * sizeof(char))));
    memcpy(str, test_str, ARRAY_LENGTH(test_str) * sizeof(char));
    assert(STRING_EQUAL(test_str, vector.data));

    assert(!utl_unsized_vector_shrink(&vector, sizeof(char) * 4, vector.size * 2));
    assert(!utl_unsized_vector_shrink(&vector, vector.size * 2, sizeof(char) * 6));

    assert(utl_unsized_vector_shrink(&vector, sizeof(char) * 3, sizeof(char) * 6));
    assert(STRING_EQUAL(test_str2, vector.data));

    utl_unsized_vector_deinit(&vector);

    assert(STRING_EQUAL(test_str, utl_unsized_vector_slice(test_str, sizeof(test_str)).data));

    assert(STRING_EQUAL(test_str2, utl_unsized_vector_literal(test_str2).data));
}
