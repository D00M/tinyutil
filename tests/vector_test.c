#include "tests.h"
#include "util.h"
#include <stdint.h>
#include <utl/vector.h>

static uint32_t test_data[] = {13, 15, 83, 90, 32, 0, 75, 3, 4, 8, 1};

static void check_array(const struct utl_vector *vec, uint32_t len, uint32_t arr[len]) {
    uint32_t *num;
    utl_vector_for_each(num, vec) {
        assert(*num == test_data[utl_vector_element_index(vec, num)]);
    }

    utl_vector_for_each_rev(num, vec) {
        assert(*num == test_data[utl_vector_element_index(vec, num)]);
    }
}

static int64_t uint32_t_cmp(const void *left, const void *right) {
    uint32_t num1 = *(const uint32_t*)left;
    uint32_t num2 = *(const uint32_t*)right;

    if (num1 < num2) return -1;
    if (num1 > num2) return 1;
    return 0;
}

static void uint32_t_destroy(void *data) {
    for (int i = 0; i < ARRAY_LENGTH(test_data); i++) {
        if (test_data[i] != *(uint32_t*)data) continue;
        return;
    }
    assert_with(false, "We failed to find num in vector_data during destruction");
}

void utl_vector_test(void) {
    struct utl_vector vector; // uint32_t
    assert_with(utl_vector_init(&vector, sizeof(uint32_t)), "failed to initialize vector");

    for (int i = 0; i < ARRAY_LENGTH(test_data); i++) {
        assert(utl_vector_push(&vector, &test_data[i]));
    }

    // Make sure the data is added correctly.
    // Also that we can iterate through the data.
    check_array(&vector, ARRAY_LENGTH(test_data), test_data);

    // Make sure swap works
    utl_vector_swap(&vector, 0, 1);
    assert_with(*(uint32_t*)utl_vector_get(&vector, 0) == test_data[1], "element 0 should be element 1");
    assert_with(*(uint32_t*)utl_vector_get(&vector, 1) == test_data[0], "element 1 should be element 0");

    // Make sure add works
    uint32_t *num = utl_vector_add(&vector);
    assert_with(num, "add should not return NULL");
    *num = 3;

    num = utl_vector_get(&vector, utl_vector_length(&vector) - 1);
    assert_with(num && *num == 3, "num should not be NULL, and should be 3");

    uint32_t length = utl_vector_length(&vector);
    utl_vector_remove(&vector, utl_vector_length(&vector) - 1);
    assert_with(utl_vector_length(&vector) == length - 1, "vector.length should have decremented after remove");
    assert_with(*(uint32_t*)utl_vector_get(&vector, utl_vector_length(&vector) - 1) != 3, "the element at the end should now not be 3");

    num = &test_data[2];
    int32_t index = utl_vector_cmp_find(&vector, uint32_t_cmp, num);
    assert_with(index != -1 && index == 2, "the index should not be -1 and should be 2");

    // Clear the vector.
    vector.vec.size = 0;
    for (int i = 0; i < ARRAY_LENGTH(test_data); i++) {
        assert(utl_vector_insert(&vector, 0, &test_data[i]));
    }

    for (int i = ARRAY_LENGTH(test_data) - 1; i >= 0; i--) {
        assert_with(*(uint32_t*)utl_vector_get(&vector, utl_vector_length(&vector) - 1 - i) == test_data[i], "With the way we inserted the vector should be a reversed list of vector_data");
    }

    assert(utl_vector_shrink(&vector, 0, utl_vector_length(&vector) / 2));

    for (int i = (ARRAY_LENGTH(test_data)/2); i >= 0; i--) {
        assert(*(uint32_t*)utl_vector_get(&vector, (utl_vector_length(&vector) - 1 - i)) == test_data[i]);
    }

    utl_vector_for_each_rev(num, &vector) {
        assert(utl_vector_remove(&vector, utl_vector_element_index(&vector, num)));
    }

    utl_vector_elements_deinit(&vector, uint32_t_destroy);

    // Check and make sure the slice and literal macros work.
    check_array(&utl_vector_slice(test_data, ARRAY_LENGTH(test_data)), 
                ARRAY_LENGTH(test_data), test_data);

    check_array(&utl_vector_literal(((uint32_t[]){13, 15, 83, 90, 32, 0, 75, 3, 4, 8, 1})),
                ARRAY_LENGTH(test_data), test_data);
}
