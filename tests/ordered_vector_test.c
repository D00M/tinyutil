#include "tests.h"
#include "util.h"
#include <utl/ordered_vector.h>

static uint32_t test_data_sorted_half[] = {13, 15, 32, 83, 90},
                test_data[] = {13, 15, 83, 90, 32, 0, 75, 3, 4, 8, 1},
                test_data_sorted[] = {0, 1, 3, 4, 8, 13, 15, 32, 75, 83, 90};

static int64_t uint32_t_cmp(const void *left, const void *right) {
    uint32_t num1 = *(const uint32_t*)left;
    uint32_t num2 = *(const uint32_t*)right;

    if (num1 < num2) return -1;
    if (num1 > num2) return 1;
    return 0;
}

// Since the ordered vector just extends functionality of the regular vector
// all this does is make sure the default sorting and search functions work.
void utl_ordered_vector_test(void) {
    struct utl_ordered_vector vector; // uint32_t
    assert(utl_ordered_vector_init(&vector, uint32_t_cmp, sizeof(uint32_t)));

    for (int i = 0; i < (ARRAY_LENGTH(test_data_sorted_half)); i++) {
        assert(utl_ordered_vector_push(&vector, &test_data[i]));
    }

    for (int i = 0; i < (ARRAY_LENGTH(test_data_sorted_half)); i++) {
        assert_with(test_data_sorted_half[i] == *(uint32_t*)utl_ordered_vector_get(&vector, i), "the data should have been sorted on insert");
    }

    // We insert the rest of the data manually then sort it
    for (int i = ARRAY_LENGTH(test_data_sorted_half); i < ARRAY_LENGTH(test_data); i++) {
        assert(utl_vector_push(&vector.vector, &test_data[i]));
    }

    utl_ordered_vector_sort(&vector);

    for (int i = 0; i < ARRAY_LENGTH(test_data_sorted); i++) {
        assert_with(test_data_sorted[i] == *(uint32_t*)utl_ordered_vector_get(&vector, i), "all of the data should be sorted");
    }

    utl_ordered_vector_deinit(&vector);
}
