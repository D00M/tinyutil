#include <utl/hash_table.h>
#include "tests.h"
#include "util.h"

static uint32_t data[] = {13, 15, 83, 90, 32, 0, 75, 3, 4, 8, 1};
static bool hash_table_destroy_iterated = false;

void uint32_t_destroy_hash_table(void *data) {
    hash_table_destroy_iterated = true;
}

void utl_hash_table_test(void) {
    struct utl_hash_table table;
    assert(utl_hash_table_init(&table));

    for (int i = 0; i < ARRAY_LENGTH(data); i++) {
        assert(utl_hash_table_insert(&table, utl_murmur3_uint32_t_hash(&data[i]), &data[i]));
    }

    for (int i = 0; i < ARRAY_LENGTH(data); i++) {
        uint32_t *ptr = utl_hash_table_get(&table, utl_murmur3_uint32_t_hash(&data[i]));
        assert_with(ptr, "data should be in hash table");
        assert_with(data[i] == *ptr, "data should match in hash table");
    }

    for (int i = 0; i < ARRAY_LENGTH(data) / 2; i++) {
        assert(utl_hash_table_remove(&table, utl_murmur3_uint32_t_hash(&data[i])));
    }

    for (int i = 0; i < ARRAY_LENGTH(data) / 2; i++) {
        assert(!utl_hash_table_get(&table, utl_murmur3_uint32_t_hash(&data[i])));
    }

    utl_hash_table_elements_deinit(&table, uint32_t_destroy_hash_table);
    assert_with(hash_table_destroy_iterated, "utl_hash_table_elements_destroy did not iterate");
}
