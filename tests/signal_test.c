#include "tests.h"
#include <utl/signal.h>

struct signal_custom_data {
    uint32_t num;
    struct utl_listener *listener2;
    struct utl_listener *listener3;
};

void signal_listener(struct utl_listener *listener, void *data) {
    struct signal_custom_data *custom = data;
    custom->num++;
    utl_list_remove(&listener->link);
    if (custom->num == 1) {
        utl_list_remove(&custom->listener2->link);
        utl_list_remove(&custom->listener3->link);
    }
}

void utl_signal_test(void) {
    struct utl_listener listener1 = {.notify = signal_listener},
                        listener2 = {.notify = signal_listener},
                        listener3 = {.notify = signal_listener},
                        listener4 = {.notify = signal_listener};

    struct utl_signal signal;
    assert(utl_signal_init(&signal));
    assert(utl_signal_add(&signal, &listener1));
    assert(utl_signal_add(&signal, &listener2));
    assert(utl_signal_add(&signal, &listener3));
    assert(utl_signal_add(&signal, &listener4));

    struct signal_custom_data data = {
        .num = 0,
        .listener2 = &listener2,
        .listener3 = &listener3,
    };
    assert(utl_signal_emit(&signal, &data));

    assert(data.num == 2);
}
